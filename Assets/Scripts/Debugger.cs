﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


namespace NimbleTrain
{
    public class Debugger : MonoBehaviour
    {
        public SaveSystem saveSystem;
        public BuyMenu buyMenu;
        private Vector3 rotated = Vector3.forward * 100f;
        //[SerializeField] private Transform handle;
        //[SerializeField] private CinemachineSmoothPath path;

        // Start is called before the first frame update


        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            /*if (Input.GetKeyDown(KeyCode.P))
            {
                //AddPath();
                //PathManager.Instance.RefreshShape();
            }*/

            if (Input.GetKeyDown(KeyCode.S))
            {
                saveSystem.OnSave();
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                saveSystem.OnLoad();
                //Debug.Break();
            }
            /*if (Input.GetKeyDown(KeyCode.B))
            {
                buyMenu.CreateLot(WagonFactory.Instance.GetWagonConfig(WagonType.Wood), 3333);
                //Debug.Break();
            }*/


            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100f)) //LayerMask.GetMask("UI")
                {

                    var lever = hit.transform.GetComponent<SwitchLever>();
                    if (lever != null)
                    {
                        lever.Toggle();
                    }
                }
            }
            /*if (Input.GetMouseButtonDown(0))
            {
                RaycastHit[] hits;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                hits = Physics.RaycastAll(ray);
                Debug.Log("Hit " + hits.Length + " objects:");
                for (int i = 0; i < hits.Length; i++)
                {
                    Debug.Log(i + ": " + hits[i].transform.gameObject.name);
                }
             }*/
        }

        /*private void OnDrawGizmos()
        {
            //we can use it to shift locomotives wagon to a new position and just place other wagons like a new train
            float closestDistance = path.FindClosestPoint(handle.position, 0, -1, 10000);
            Gizmos.DrawLine(handle.position, path.EvaluatePosition(closestDistance));
        }*/

        /*private void Load()
        {
            Train.Instance.LoadAllWagons();
        }*/

        private void AddPath()
        {
            //PathManager.Instance.GenerateNextWaypoint();
            //PathManager.Instance.GeneratePath();
        }

        private void AddLocomotive()
        {
            Locomotive locomotive = WagonFactory.Instance.CreateLocomotive(LocomotiveType.Steam1);
            Train.Instance.AddLocomotive(locomotive);
            //Wagon w = WagonFactory.Instance.CreateWagon(WagonFactory.WagonType.Cargo);
            //Train.Instance.AddWagon(w);
        }

        private void AddWagon()
        {
            Wagon w = WagonFactory.Instance.CreateWagon(WagonType.Cargo);
            Train.Instance.AddWagon(w);
        }
    } 
}
