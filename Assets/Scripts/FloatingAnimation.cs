﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace NimbleTrain
{
    public class FloatingAnimation : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            transform.DOMoveY(transform.position.y + 0.5f, 1f).SetLoops(-1, LoopType.Yoyo);
        }

    } 
}
