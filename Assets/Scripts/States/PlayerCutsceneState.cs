﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class PlayerCutsceneState : State<Player>
    {
        private Player _player;

        private float _holdToSkipDuration = 2f;
        private float _skipTimer;
        private bool _isSkipping;

        public PlayerCutsceneState(Player player) : base(player)
        {
            _player = player;
        }

        public override void Tick()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                _skipTimer = _holdToSkipDuration;
            }

            if (Input.GetKey(KeyCode.Space))
            {
                _skipTimer -= Time.deltaTime;
                if (_skipTimer <= 0f)
                {
                    _player.SkipCutscene();
                }
            }
        }

        public override void OnStateEnter()
        {
            _skipTimer = _holdToSkipDuration;
            _isSkipping = false;
        }

        public override void OnStateExit()
        {
            //...
        }
    } 
}
