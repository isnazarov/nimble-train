﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class PlayerWaitingState : State<Player>
    {
        private Player _player;

        public PlayerWaitingState(Player player) : base(player)
        {
            _player = player;
        }

        public override void Tick()
        {
            //...
        }

        /*public override void OnStateEnter()
        {
            
        }

        public override void OnStateExit()
        {
            
        }
        */
    }

}