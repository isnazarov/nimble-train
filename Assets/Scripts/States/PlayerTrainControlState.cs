﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{

    public class PlayerTrainControlState : State<Player>
    {
        private Player _player;

        public PlayerTrainControlState(Player player) : base(player)
        {
            _player = player;
        }

        public override void Tick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100f)) //LayerMask.GetMask("UI")
                {

                    var lever = hit.transform.GetComponent<SwitchLever>();
                    if (lever != null)
                    {
                        lever.Toggle();
                    }
                }
            }
            if (Input.GetKey(KeyCode.Mouse0))
            {
                _player.Train.Accelerate();
            }
            else
            {
                _player.Train.Brake();
            }
            if (Input.GetKey(KeyCode.R))
            {
                _player.Train.ResetPosition();
            }
            _player.Train.Move();
        }

        public override void OnStateEnter()
        {
            _player.isInControlState = true;
        }

        public override void OnStateExit()
        {
            _player.isInControlState = false;
        }

    }

}