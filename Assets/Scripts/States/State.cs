﻿namespace NimbleTrain
{
    public abstract class State<T>
    {
        protected T component;

        public abstract void Tick();

        public virtual void OnStateEnter() { }
        public virtual void OnStateExit() { }

        public State(T component)
        {
            this.component = component;
        }
    }

}