﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class PlayerTrainLoadingState : State<Player>
    {
        private Player _player;

        public PlayerTrainLoadingState(Player player) : base(player)
        {
            _player = player;
        }

        public override void Tick()
        {
            //...
        }

        public override void OnStateEnter()
        {
            _player.Train.AllowTilt(false);
        }

        public override void OnStateExit()
        {
            _player.Train.AllowTilt(true);
        }

    }

}