﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public class CutsceneBuildHouseTown : Cutscene
    {
        [SerializeField] private SceneReference sceneTown;
        [SerializeField] private Buildable building;

        private bool _sceneTownToUnload = false;

        public override void Prepare()
        {
            InterruptPlay();

            building.SetAsRemoved();
            building.gameObject.SetActive(false);
        }

        protected override IEnumerator Sequence()
        {
            LoadScenes();

            cameraController.SwitchToVcamTown();
            yield return new WaitForSeconds(2f);
            building.gameObject.SetActive(true);
            building.Build();
            yield return new WaitForSeconds(building.Duration + 1.5f);

            cameraController.SwitchToVcamDefault();
            yield return new WaitForSeconds(2f);

            UnloadScenes();

            _isPlaying = false;
            yield return null;
        }

        public override void SetAsPlayed()
        {
            InterruptPlay();
            building.gameObject.SetActive(true);
            building.SetAsBuilt();
        }

        protected override void LoadScenes()
        {
            _sceneTownToUnload = false;
            if (!sceneTown.Scene.isLoaded)
            {
                _sceneTownToUnload = true;
                SceneManager.LoadScene(sceneTown.Name, LoadSceneMode.Additive);
            }
        }

        protected override void UnloadScenes()
        {
            if (_sceneTownToUnload)
            {
                SceneManager.UnloadSceneAsync(sceneTown.Name);
            }
        }
    }

}