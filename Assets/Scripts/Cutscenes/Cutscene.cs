﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public abstract class Cutscene : MonoBehaviour
    {
        [SerializeField] protected CameraController cameraController;

        protected bool _isPlaying = false;
        public bool IsPlaying => _isPlaying;

        public abstract void Prepare();
        protected abstract IEnumerator Sequence();
        protected abstract void LoadScenes();
        protected abstract void UnloadScenes();
        public abstract void SetAsPlayed();

        protected void InterruptPlay()
        {
            if (_isPlaying)
            {
                StopAllCoroutines();
                _isPlaying = false;
                UnloadScenes();
                cameraController.SwitchToVcamDefault();
            }
        }

        public void Play()
        {
            _isPlaying = true;
            StartCoroutine(Sequence());
        }

        
    } 
}
