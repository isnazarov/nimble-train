﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public class CutsceneOpenQuarry : Cutscene
    {
        [SerializeField] private SceneReference sceneRiverCrossing;
        [SerializeField] private Buildable riverCrossingStoneWall;
        [SerializeField] private Buildable pathA1;

        [SerializeField] private SceneReference sceneCliff;
        [SerializeField] private Buildable cliffStoneWall;
        [SerializeField] private Buildable pathCliff02;

        [SerializeField] private SceneReference sceneLocationA;

        private bool _sceneRiverCrossingToUnload = false;
        private bool _sceneCliffToUnload = false;
        private bool _sceneLocationAToUnload = false;


        public override void Prepare()
        {
            InterruptPlay();

            riverCrossingStoneWall.SetAsBuilt();
            pathA1.SetAsRemoved();
            pathA1.gameObject.SetActive(false);
            cliffStoneWall.SetAsBuilt();
            pathCliff02.SetAsBuilt();
            pathCliff02.gameObject.SetActive(false);
        }

        protected override IEnumerator Sequence()
        {
            LoadScenes();

            cameraController.SwitchToVcamRiverCrossingEntrance();
            yield return new WaitForSeconds(2f);
            riverCrossingStoneWall.Remove();
            yield return new WaitForSeconds(riverCrossingStoneWall.Duration);
            pathA1.gameObject.SetActive(true);
            pathA1.Build();
            yield return new WaitForSeconds(pathA1.Duration + 1.5f);

            cameraController.SwitchToVcamCliffExit();
            yield return new WaitForSeconds(1f);
            cliffStoneWall.Remove();
            yield return new WaitForSeconds(cliffStoneWall.Duration);
            pathCliff02.gameObject.SetActive(true);
            pathCliff02.Build();
            yield return new WaitForSeconds(pathCliff02.Duration + 1.5f);

            cameraController.SwitchToVcamDefault();
            yield return new WaitForSeconds(2f);

            UnloadScenes();

            _isPlaying = false;
            yield return null;
        }

        public override void SetAsPlayed()
        {
            InterruptPlay();

            riverCrossingStoneWall.SetAsRemoved();
            pathA1.gameObject.SetActive(true);
            pathA1.SetAsBuilt();
            cliffStoneWall.SetAsRemoved();
            pathCliff02.gameObject.SetActive(true);
            pathCliff02.SetAsBuilt();
        }

        protected override void LoadScenes()
        {
            _sceneRiverCrossingToUnload = false;
            _sceneCliffToUnload = false;
            _sceneLocationAToUnload = false;
            if (!sceneRiverCrossing.Scene.isLoaded)
            {
                _sceneRiverCrossingToUnload = true;
                SceneManager.LoadScene(sceneRiverCrossing.Name, LoadSceneMode.Additive);
            }

            if (!sceneCliff.Scene.isLoaded)
            {
                _sceneCliffToUnload = true;
                SceneManager.LoadScene(sceneCliff.Name, LoadSceneMode.Additive);
            }

            if (!sceneLocationA.Scene.isLoaded)
            {
                _sceneLocationAToUnload = true;
                SceneManager.LoadScene(sceneLocationA.Name, LoadSceneMode.Additive);
            }
        }

        protected override void UnloadScenes()
        {
            if (_sceneRiverCrossingToUnload)
            {
                SceneManager.UnloadSceneAsync(sceneRiverCrossing.Name);
            }
            if (_sceneCliffToUnload)
            {
                SceneManager.UnloadSceneAsync(sceneCliff.Name);
            }
            if (_sceneLocationAToUnload)
            {
                SceneManager.UnloadSceneAsync(sceneLocationA.Name);
            }
        }
        
    } 
}
