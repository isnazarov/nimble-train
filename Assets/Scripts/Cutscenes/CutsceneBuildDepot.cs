﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{

    public class CutsceneBuildDepot : Cutscene
    {

        [SerializeField] private SceneReference sceneDepot;
        [SerializeField] private Buildable depot;
        [SerializeField] private Buildable pathDepot;
        [SerializeField] private Buildable pathDepot02;

        private bool _sceneDepotToUnload = false;


        public override void Prepare()
        {
            InterruptPlay();

            depot.SetAsRemoved();
            depot.gameObject.SetActive(false);
            pathDepot.SetAsRemoved();
            pathDepot.gameObject.SetActive(false);
            pathDepot02.SetAsRemoved();
            pathDepot02.gameObject.SetActive(false);
        }

        protected override IEnumerator Sequence()
        {
            LoadScenes();

            cameraController.SwitchToVcamDepot();
            yield return new WaitForSeconds(2f);
            pathDepot.gameObject.SetActive(true);
            pathDepot.Build();
            pathDepot02.gameObject.SetActive(true);
            pathDepot02.Build();
            yield return new WaitForSeconds(pathDepot.Duration + 1.5f);
            depot.gameObject.SetActive(true);
            depot.Build();
            yield return new WaitForSeconds(depot.Duration + 1.5f);
           
            cameraController.SwitchToVcamDefault();
            yield return new WaitForSeconds(2f);
            
            UnloadScenes();

            _isPlaying = false;
            yield return null;
        }

        public override void SetAsPlayed()
        {
            InterruptPlay();

            depot.gameObject.SetActive(true);
            depot.SetAsBuilt();
            pathDepot.gameObject.SetActive(true);
            pathDepot.SetAsBuilt();
            pathDepot02.gameObject.SetActive(true);
            pathDepot02.SetAsBuilt();
            
        }

        protected override void LoadScenes()
        {
            _sceneDepotToUnload = false;
            if (!sceneDepot.Scene.isLoaded)
            {
                _sceneDepotToUnload = true;
                SceneManager.LoadScene(sceneDepot.Name, LoadSceneMode.Additive);
            }
        }

        protected override void UnloadScenes()
        {
            if (_sceneDepotToUnload)
            {
                SceneManager.UnloadSceneAsync(sceneDepot.Name);
            }
        }
    } 
}
