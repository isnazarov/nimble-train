﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour
{
    public UnityEvent _event;

    private void OnTriggerEnter(Collider other)
    {
        if (_event != null && other.gameObject.layer == LayerMask.NameToLayer("Detector"))
        {
            _event.Invoke();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawCube(transform.position, transform.localScale);

        GUIStyle labelStyle = new GUIStyle();
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.alignment = TextAnchor.MiddleCenter;
        UnityEditor.Handles.Label(transform.position + Vector3.up * 2f, gameObject.name, labelStyle);
    }
#endif

}
