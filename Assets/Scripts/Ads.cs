﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.Events;
using System;

namespace NimbleTrain
{
    public class Ads : MonoBehaviour
    {
        public static Ads Instance { get; private set; }

        private InterstitialAd interstitial;

        [SerializeField] private UnityEvent onAdOpened;
        [SerializeField] private UnityEvent onAdClosed;

        [HideInInspector] public bool isPendingToPlay = false;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            MobileAds.Initialize(initStatus => { });
        }

        private void OnEnable()
        {
            RequestInterstitial();
        }

        private void OnDisable()
        {
            if (this.interstitial != null)
            {
                this.interstitial.Destroy();
            }
            
        }

        private void RequestInterstitial()
        {
            #if UNITY_ANDROID
            string adUnitId = "ca-app-pub-3940256099942544/1033173712";
            #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/4411468910";
            #else
            string adUnitId = "unexpected_platform";
            #endif

            if (this.interstitial != null)
            {
                this.interstitial.Destroy();
            }

            // Initialize an InterstitialAd.
            this.interstitial = new InterstitialAd(adUnitId);

            this.interstitial.OnAdOpening += HandleOnAdOpening;
            this.interstitial.OnAdClosed += HandleOnAdClosed;

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            this.interstitial.LoadAd(request);
        }

        private void HandleOnAdOpening(object sender, EventArgs args)
        {
            Time.timeScale = 0f;
            onAdOpened.Invoke();
        }

        private void HandleOnAdClosed(object sender, EventArgs args)
        {
            Time.timeScale = 1f;
            onAdClosed.Invoke();
        }

        public void ShowInterstitialAd()
        {
            if (this.interstitial.IsLoaded() && isPendingToPlay)
            {
                this.interstitial.Show();
                isPendingToPlay = false;
            }
        }
    } 
}
