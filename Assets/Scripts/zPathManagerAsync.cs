﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Threading;
using UnityEngine.U2D;


namespace NimbleTrain
{
    // !!! Doesn't work because operations with MonoBehaviour objects should be performed only in main thread  
    // Maybe see later.

    public class PathManagerAsync : MonoBehaviour
    {
        [SerializeField] private CinemachineSmoothPath pathPrefab;
        [SerializeField] private SpriteShapeController railwaySpriteShapePrefab;
        [SerializeField, Range(10, 100)] private int pathSegments;
        [SerializeField] private int spriteShapeResolution = 10;
        [SerializeField, Range(10f, 100f)] private float pathSegmentLengthFactor;
        [SerializeField, Range(10f, 120f)] private float pathTurnAngleFactor;

        public CinemachineSmoothPath currentPath;
        private CinemachineSmoothPath nextPath;
        private SpriteShapeController currentSpriteShape;
        private SpriteShapeController nextSpriteShape;

        public CinemachineSmoothPath NextPath { get { return nextPath; } }

        bool threadGenerateNextPathIsRunning;
        Thread threadGenerateNextPath;

        public CinemachineSmoothPath CurrentPath
        {
            get
            {
                if (currentPath == null)
                {
                    GeneratePath();
                }
                return currentPath;
            }
        }

        public static PathManagerAsync Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            threadGenerateNextPath = new Thread(GenerateNextPath);
        }

        // Start is called before the first frame update
        void Start()
        {
            //GeneratePath();
            //GenerateSpriteShape(currentPath);
            //GenerateNextWaypoint(currentPath);

        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(Time.deltaTime);
        }

        private void OnDisable()
        {
            if (threadGenerateNextPathIsRunning)
            {
                threadGenerateNextPathIsRunning = false;
                threadGenerateNextPath.Join();
            }
        }

        public void GeneratePath()
        {
            //thread.Start();
            if (currentPath == null)
            {
                currentPath = Instantiate(pathPrefab);
                currentPath.transform.SetParent(transform);
                InitialPath();
                currentSpriteShape = GenerateSpriteShape(currentPath);
            }
            else
            {
                if (nextPath == null)
                {
                    nextPath = Instantiate(pathPrefab);
                    nextPath.transform.SetParent(transform);
                    //GenerateNextPath();
                    threadGenerateNextPath.Start();
                    nextSpriteShape = GenerateSpriteShape(nextPath);
                }
                Destroy(currentPath.gameObject);
                Destroy(currentSpriteShape.gameObject);
                currentPath = nextPath;
                currentSpriteShape = nextSpriteShape;
                nextPath = Instantiate(pathPrefab);
                nextPath.transform.SetParent(transform);
            }
            //GenerateNextPath();
            threadGenerateNextPath.Start();
            nextSpriteShape = GenerateSpriteShape(nextPath);
        }

        // DOESN'T WORK BECAUSE CAN INSTANTIATE ONLY INSIDE MAIN LOOP
        /*private void GeneratePathAsync()
        {
            threadRunning = true;
            // bool workDone = false;
            // This pattern lets us interrupt the work at a safe point if neeeded.
            //while (_threadRunning && !workDone)
            //{
                // Do Work...
            //}
            if (currentPath == null)
            {
                currentPath = StraightPath();
                currentSpriteShape = GenerateSpriteShape(currentPath);
            }
            else
            {
                if (nextPath == null)
                {
                    GenerateNextPath();
                    nextSpriteShape = GenerateSpriteShape(nextPath);
                }
                Destroy(currentPath.gameObject);
                Destroy(currentSpriteShape.gameObject);
                currentPath = nextPath;
                currentSpriteShape = nextSpriteShape;
            }
            GenerateNextPath();
            nextSpriteShape = GenerateSpriteShape(nextPath);
            threadRunning = false;
        }*/

        private void InitialPath()
        {
            InitialPath(Vector3.zero, Vector3.forward);
        }

        private void InitialPath(Vector3 startPos, Vector3 direction)
        {
            //thread.Start();

            direction.Normalize();
            //CinemachineSmoothPath path = Instantiate(pathPrefab);
            //path.transform.SetParent(transform);
            Vector3 pos = startPos;
            var list = new List<CinemachineSmoothPath.Waypoint>();
            for (int i = 0; i < pathSegments; i++)
            {
                var wp = new CinemachineSmoothPath.Waypoint();
                wp.position = pos;
                //wp.tangent = tangent;
                list.Add(wp);
                pos += direction * pathSegmentLengthFactor;
            }
            currentPath.m_Waypoints = list.ToArray();
            currentPath.InvalidateDistanceCache();
            //return path;
        }

        private void GenerateNextPath()
        {
            //float debugTime = Time.realtimeSinceStartup;
            //nextPath = Instantiate(pathPrefab);
            //nextPath.transform.SetParent(transform);
            threadGenerateNextPathIsRunning = true;

            var list = new List<CinemachineSmoothPath.Waypoint>();
            Vector3 tangent = currentPath.EvaluateTangentAtUnit(1f, CinemachinePathBase.PositionUnits.Normalized).normalized;
            //list.Add(currentPath.m_Waypoints[currentPath.m_Waypoints.Length - 1]);
            for (int i = 0; i < 4; i++)
            {
                Vector3 newPosition = currentPath.m_Waypoints[currentPath.m_Waypoints.Length - 1].position - currentPath.transform.position + tangent * pathSegmentLengthFactor * i;
                var wp = new CinemachineSmoothPath.Waypoint();
                wp.position = newPosition;
                list.Add(wp);
            }
            nextPath.m_Waypoints = list.ToArray();
            nextPath.InvalidateDistanceCache();
            // !!! think how to add other points not recalculating each time
            for (int i = 0; i < pathSegments - 4; i++)
            {
                GenerateNextWaypoint(nextPath);
            }
            //Debug.Log(Time.realtimeSinceStartup - debugTime);
            //Debug.Log(Time.deltaTime);
            threadGenerateNextPathIsRunning = false;
        }

        public void GenerateNextWaypoint()
        {
            GenerateNextWaypoint(currentPath);
        }

        public void GenerateNextWaypoint(CinemachineSmoothPath path)
        {
            Vector3 tangent = path.EvaluateTangentAtUnit(1f, CinemachinePathBase.PositionUnits.Normalized).normalized;
            float angle = Random.Range(-pathTurnAngleFactor, pathTurnAngleFactor);
            Vector3 rotatedTangent = Quaternion.AngleAxis(angle, Vector3.up) * tangent * pathSegmentLengthFactor;
            //Vector3 newPosition = path.EvaluatePositionAtUnit(1f, CinemachinePathBase.PositionUnits.Normalized) - path.transform.position + rotatedTangent;
            Vector3 newPosition = path.m_Waypoints[path.m_Waypoints.Length - 1].position - path.transform.position + rotatedTangent;
            var list = new List<CinemachineSmoothPath.Waypoint>();
            for (int i = 0; i < path.m_Waypoints.Length; i++)
            {
                list.Add(path.m_Waypoints[i]);
            }
            var wp = new CinemachineSmoothPath.Waypoint();
            wp.position = newPosition;
            list.Add(wp);
            path.m_Waypoints = list.ToArray();
            path.InvalidateDistanceCache();
            //UpdateSpriteShape(path);
        }


        //
        //Sprite Shape methods
        //

        // Maybe move spriteShape methods to separate static class?
        private SpriteShapeController GenerateSpriteShape(CinemachineSmoothPath path)
        {
            //float debugTime = Time.realtimeSinceStartup;
            SpriteShapeController shape = Instantiate(railwaySpriteShapePrefab);
            shape.transform.SetParent(transform);
            shape.spline.Clear();
            int index = 0;
            for (int i = 0; i < path.m_Waypoints.Length; i++)
            {
                for (int j = 0; j < spriteShapeResolution; j++)
                {
                    float d = i + 0.1f * j;
                    Vector3 position = path.EvaluatePositionAtUnit(d, CinemachinePathBase.PositionUnits.PathUnits);
                    Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                    shape.spline.InsertPointAt(index, orientedPosition);
                    index++;
                    if (i == path.m_Waypoints.Length - 1)
                    {
                        break;
                    }
                }
            }

            for (int i = 1; i < shape.spline.GetPointCount() - 1; i++)
            {
                Smoothen(shape, i);
            }
            //shape.RefreshSpriteShape();
            //Debug.Log(Time.realtimeSinceStartup - debugTime);
            return shape;
        }


        private void UpdateSpriteShape(CinemachineSmoothPath path)
        {
            int startIndex = currentSpriteShape.spline.GetPointCount() - 1;
            int index = startIndex;
            for (float pathUnits = path.m_Waypoints.Length - 2; pathUnits <= path.m_Waypoints.Length - 1f; pathUnits += 0.1f)
            {
                //float closestDistance = path.FindClosestPoint(handle.position, 0, -1, 10000);
                //Gizmos.DrawLine(handle.position, path.EvaluatePosition(closestDistance));
                Vector3 position = path.EvaluatePositionAtUnit(pathUnits, CinemachinePathBase.PositionUnits.PathUnits);
                Debug.Log("PathUnits = " + pathUnits.ToString() + " " + position);
                Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                index++;
                currentSpriteShape.spline.InsertPointAt(index, orientedPosition);
            }
            //spriteShape.spline.RemovePointAt(startIndex);
            for (int i = startIndex; i < currentSpriteShape.spline.GetPointCount(); i++)
            {
                Smoothen(currentSpriteShape, i);
            }

            /*for (int i = 0; i < 10; i++)
            {
                spriteShape.spline.RemovePointAt(0);
            }*/

            //spriteShape.RefreshSpriteShape();
        }



        private void Smoothen(SpriteShapeController sc, int pointIndex)
        {
            Vector3 position = sc.spline.GetPosition(pointIndex);
            Vector3 positionNext = sc.spline.GetPosition(SplineUtility.NextIndex(pointIndex, sc.spline.GetPointCount()));
            Vector3 positionPrev = sc.spline.GetPosition(SplineUtility.PreviousIndex(pointIndex, sc.spline.GetPointCount()));
            Vector3 forward = gameObject.transform.forward;

            float scale = Mathf.Min((positionNext - position).magnitude, (positionPrev - position).magnitude) * 0.33f;

            Vector3 leftTangent = (positionPrev - position).normalized * scale;
            Vector3 rightTangent = (positionNext - position).normalized * scale;

            sc.spline.SetTangentMode(pointIndex, ShapeTangentMode.Continuous);
            SplineUtility.CalculateTangents(position, positionPrev, positionNext, forward, scale, out rightTangent, out leftTangent);

            sc.spline.SetLeftTangent(pointIndex, leftTangent);
            sc.spline.SetRightTangent(pointIndex, rightTangent);
        }

        /*private void OnDrawGizmos()
        {
            Vector3 tangent = currentPath.EvaluateTangentAtUnit(10f, CinemachinePathBase.PositionUnits.PathUnits).normalized * 100f;
            Gizmos.DrawLine(currentPath.transform.position + currentPath.m_Waypoints[10].position, currentPath.transform.position + currentPath.m_Waypoints[10].position + tangent);
            //Gizmos.DrawSphere(currentPath.transform.position + currentPath.m_Waypoints[10].position + tangent, 3f);
            //Vector3 newPosition = tangent.normalized * 100f;
            //newPosition = Quaternion.AngleAxis(Random.Range(-60f, 60f), Vector3.up) * newPosition;
            //Gizmos.DrawLine(currentPath.transform.position + currentPath.m_Waypoints[10].position, currentPath.transform.position + currentPath.m_Waypoints[10].position + newPosition);
        }*/

    }

}