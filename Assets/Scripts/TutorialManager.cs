﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public static TutorialManager Instance { get; private set; }

    [SerializeField] private TutorialMessage tutorialMessage;

    public OneTimeEventTrigger railroadSwitchTutorialTrigger;

    public bool startOver = true; 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        if (startOver)
        {
            ShowBasics();
        }
    }

    public void ShowBasics()
    {
        tutorialMessage.Show("Tap and hold to accelerate.\nRelease to brake.\nSlow down on turns!!!");
    }

    public void ActivateRailroadSwitchTutorial()
    {
        railroadSwitchTutorialTrigger.gameObject.SetActive(true);
    }

    public void ShowRailroadSwitchMessage()
    {
        tutorialMessage.Show("Click on railroad switch to change the path!\nYou can visit the depot to buy more wagons!");
    }
}
