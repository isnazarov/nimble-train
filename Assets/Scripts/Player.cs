﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NimbleTrain
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private Train _train;
        public Train Train => _train;

        [SerializeField] private GameSession _gameSession;
        public GameSession GameSession => _gameSession;

        private PlayerTrainControlState _trainControlState;
        private PlayerTrainLoadingState _trainLoadingState;
        private PlayerCutsceneState _cutsceneState;
        private PlayerWaitingState _waitingState;
        private State<Player> _currentState;

        [SerializeField] private UnityEvent EventCutsceneSkip;

        public bool isInControlState = false;

        public static Player Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            _trainControlState = new PlayerTrainControlState(this);
            _trainLoadingState = new PlayerTrainLoadingState(this);
            _cutsceneState = new PlayerCutsceneState(this);
            _waitingState = new PlayerWaitingState(this);
        }

        void Update()
        {
            _currentState?.Tick();
        }

        public void SetState(State<Player> state)
        {
            if (_currentState != null)
                _currentState.OnStateExit();

            _currentState = state;

            if (_currentState != null)
                _currentState.OnStateEnter();
        }

        public void SetLoadingState()
        {
            SetState(_trainLoadingState);
        }

        public void SetControlState()
        {
            SetState(_trainControlState);
        }

        public void SetCutsceneState()
        {
            SetState(_cutsceneState);
        }

        public void SkipCutscene()
        {
            EventCutsceneSkip.Invoke();
        }

        public void SetWaitingState()
        {
            SetState(_waitingState);
        }

    } 
}
