﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class SceneReference : ScriptableObject
{
    public string Name;

    public Scene Scene
    {
        get
        {
            return SceneManager.GetSceneByName(Name);
        }
    }
}
