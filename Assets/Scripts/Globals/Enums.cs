﻿namespace NimbleTrain
{
    public enum Direction { left, right };

    [System.Serializable]
    public enum CargoType {
        wood,
        stone
    };
}