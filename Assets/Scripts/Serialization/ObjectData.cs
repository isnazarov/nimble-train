﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NimbleTrain
{
    [System.Serializable]
    public class SaveData
    {
        private static SaveData _current;
        public static SaveData current
        {
            get
            {
                if (_current == null)
                {
                    _current = new SaveData();
                }
                return _current;
            }
            set  //We need set method to avoid error: "Property or indexer 'SaveData.current' cannot be assigned to -- it is read only"
            {
                if (value != null)
                {
                    _current = value;
                }
            }
        }

        public GameSessionData gameSessionData;
        public TrainData trainData;
        public BuyMenuData buyMenuData;
        public TutorialData tutorialData;
    }

    [System.Serializable]
    public class GameSessionData
    {
        public int money;
        public int currentWinCondition;
        public Dictionary<CargoType, int> deliveredCargo;
        public Dictionary<string, bool> loadedScenes;
    }

    [System.Serializable]
    public class TrainData
    {
        //public LocomotiveType locomotiveType;
        public List<WagonData> wagons; // for a while we consider wagons[0] as a locomotive

        public string currentPathName; // GameObject.Find(currentPathName)
        public bool isReversed;
        public float position;
    }


    [System.Serializable]
    public class WagonData
    {
        public WagonType wagonType;
        public CargoCountableData cargoCountable;
        public CargoUncountableData cargoUncountable;

        public Vector3 position;
        public Quaternion rotation;
        public float distance;
        public string pathName;
        public bool IsReverseDirection;
    }

    [System.Serializable]
    public class CargoCountableData
    {
        public CargoType cargoType;
        public List<CargoUnitData> cargoUnits;
    }

    [System.Serializable]
    public class CargoUnitData
    {
        public Vector3 position;
        public Quaternion rotation;
    }

    [System.Serializable]
    public class CargoUncountableData
    {
        public CargoType cargoType;
        public float currentCapacity; 
    }

    [System.Serializable]
    public class BuyMenuData
    {
        public List<LotData> lots;
    }

    [System.Serializable]
    public class LotData
    {
        public WagonType wagonType;
        public int price;
    }

    [System.Serializable]
    public class TutorialData
    {
        public List<bool> eventTriggers;
    }
}