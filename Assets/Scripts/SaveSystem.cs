﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;


namespace NimbleTrain
{
    public class SaveSystem : MonoBehaviour
    {
        public static SaveSystem Instance { get; private set; }

        [SerializeField] private GameSession gameSession;
        [SerializeField] private Train train;
        [SerializeField] private PathManager pathManager;
        [SerializeField] private BuyMenu buyMenu;
        [SerializeField] private TutorialManager tutorialManager;

        [SerializeField] private string saveFileName;
        public bool autoLoad;
        public bool autoSave;
        [SerializeField] private float autoSavePeriod = 30f;
        private float _autoSaveTimer = 0f;
        private bool _autoSaveTimerIsPaused = false;

        private Scene _activeScene;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            if (autoLoad)
            {
                if (File.Exists(Application.persistentDataPath + "/saves/" + saveFileName + ".save"))
                {
                    OnLoad();
                }
                else
                {
                    StartNewGame();
                }
            }
            else
            {
                StartNewGame();
            }
        }

        private void Update()
        {
            if (autoSave)
            {
                if (!_autoSaveTimerIsPaused && Player.Instance.isInControlState)
                {
                    _autoSaveTimer += Time.deltaTime;
                    if (_autoSaveTimer >= autoSavePeriod)
                    {
                        _autoSaveTimer = 0f;
                        OnSave();
                    }
                }
                /*else
                {
                    _autoSaveTimer = 0f;
                }*/
            }
            
        }

        // it doesnt't called on android device
        /*private void OnApplicationQuit()
        {
            OnSave();
        }*/

        public void StartNewGame()
        {
            gameSession.StartNewGame();
        }

        public void PauseAutosaveTimer()
        {
            _autoSaveTimerIsPaused = true;
        }

        public void UnpauseAutosaveTimer()
        {
            _autoSaveTimerIsPaused = false;
        }

        public void OnSave()
        {
            // we update all save data here
            SaveData.current.gameSessionData = new GameSessionData();
            SaveData.current.gameSessionData.money = gameSession.GetMoney();
            SaveData.current.gameSessionData.currentWinCondition = gameSession.CurrentWinCondition;
            SaveData.current.gameSessionData.deliveredCargo = gameSession.deliveredCargo;
            SaveData.current.gameSessionData.loadedScenes = new Dictionary<string, bool>();
            foreach (SceneReference sceneRef in gameSession.subscenes)
            {
                SaveData.current.gameSessionData.loadedScenes.Add(sceneRef.Name, sceneRef.Scene.isLoaded);
            }

            SaveData.current.trainData = new TrainData();
            SaveData.current.trainData.wagons = new List<WagonData>();
            foreach (Wagon w in Train.Instance.Wagons)
            {
                WagonData wagonData = new WagonData();
                wagonData.wagonType = w.Config.type;
                wagonData.cargoCountable = GetCargoCountableData(w.GetComponent<CargoCountable>());
                wagonData.cargoUncountable = GetCargoUncountableData(w.GetComponent<CargoUncountable>());
                wagonData.position = w.Hull.transform.position;
                wagonData.rotation = w.Hull.transform.rotation;
                wagonData.distance = w.Position;
                wagonData.pathName = w.Path.gameObject.name;
                wagonData.IsReverseDirection = w.IsReverseDirection;
                SaveData.current.trainData.wagons.Add(wagonData);
            }
            SaveData.current.trainData.currentPathName = pathManager.CurrentPath.gameObject.name;
            SaveData.current.trainData.isReversed = pathManager.IsReverseDirection;
            SaveData.current.trainData.position = train.Wagons[0].Position;

            SaveData.current.buyMenuData = new BuyMenuData();
            SaveData.current.buyMenuData.lots = new List<LotData>();
            foreach(Transform t in buyMenu.lotsParent)
            {
                Lot lot = t.GetComponent<Lot>();
                if (lot != null)
                {
                    LotData lotData = new LotData();
                    lotData.wagonType = lot.Config.type;
                    lotData.price = lot.Price;
                    SaveData.current.buyMenuData.lots.Add(lotData);
                }
            }

            SaveData.current.tutorialData = new TutorialData();
            SaveData.current.tutorialData.eventTriggers = new List<bool>();
            foreach (Transform eventTriggerTransform in tutorialManager.transform)
            {
                SaveData.current.tutorialData.eventTriggers.Add(eventTriggerTransform.gameObject.activeInHierarchy);
            }

            SerializationManager.Save(saveFileName, SaveData.current);
        }

        private CargoUncountableData GetCargoUncountableData(CargoUncountable cargoUncountable)
        {
            if (cargoUncountable == null)
            {
                return null;
            }
            CargoUncountableData data = new CargoUncountableData();
            data.cargoType = cargoUncountable.Type;
            data.currentCapacity = cargoUncountable.CurrentCapacity;
            return data;
        }

        private CargoCountableData GetCargoCountableData(CargoCountable cargoCountable)
        {
            if (cargoCountable == null)
            {
                return null;
            }
            CargoCountableData data = new CargoCountableData();
            data.cargoType = cargoCountable.Type;
            data.cargoUnits = new List<CargoUnitData>();
            foreach (var cargoUnit in cargoCountable.cargoUnits)
            {
                CargoUnitData cargoUnitData = new CargoUnitData();
                cargoUnitData.position = cargoUnit.transform.position;
                cargoUnitData.rotation = cargoUnit.transform.rotation;
                data.cargoUnits.Add(cargoUnitData);
            }
            return data;
        }




        public void OnLoad()
        {
            //_activeScene = SceneManager.GetActiveScene();
            //SceneManager.sceneLoaded += OnActiveSceneLoaded;
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

            Train.Instance.Clear();

            SaveData.current = (SaveData)SerializationManager.Load(Application.persistentDataPath + "/saves/" + saveFileName + ".save");

            // load game session parameters
            gameSession.SetMoney(SaveData.current.gameSessionData.money);
            gameSession.deliveredCargo = SaveData.current.gameSessionData.deliveredCargo;
            gameSession.SetCurrentWinCondition(SaveData.current.gameSessionData.currentWinCondition); // should run after setting delivered cargo for updating cargo task panel properly
            foreach (SceneReference sceneRef in gameSession.subscenes)
            {
                if (SaveData.current.gameSessionData.loadedScenes[sceneRef.Name])
                {
                    if (!sceneRef.Scene.isLoaded)
                    {
                        SceneManager.LoadScene(sceneRef.Name, LoadSceneMode.Additive);
                    }
                }
                else
                {
                    if (sceneRef.Scene.isLoaded)
                    {
                        SceneManager.UnloadSceneAsync(sceneRef.Name);
                    }
                }
            }

            // load path parameters
            pathManager.SetNextActivePath(GameObject.Find(SaveData.current.trainData.currentPathName).GetComponent<Path>(), SaveData.current.trainData.isReversed);

            // load train parameters
            train._startPosition = SaveData.current.trainData.position;
            //List<CargoCountableData> cargoCountableDataToLoad = new List<CargoCountableData>();
            foreach (WagonData wdata in SaveData.current.trainData.wagons)
            {
                Wagon w = WagonFactory.Instance.CreateWagon(wdata.wagonType);
                w.Hull.transform.SetPositionAndRotation(wdata.position, wdata.rotation);
                w.Path = GameObject.Find(wdata.pathName).GetComponent<Path>();
                w.IsReverseDirection = wdata.IsReverseDirection;
                w.Platform.SetCartPosition(wdata.distance);
                train.AddWagon(w);
                if (wdata.cargoUncountable != null)
                {
                    w.GetComponent<CargoUncountable>().SetCapacity(wdata.cargoUncountable.currentCapacity);
                    //Debug.Break();
                }

                //Placing cargo units before placing the train
                if (wdata.cargoCountable != null)
                {
                    CargoCountable cargoCountable = w.GetComponent<CargoCountable>();
                    foreach (CargoUnitData cargoUnitData in wdata.cargoCountable.cargoUnits)
                    {
                        cargoCountable.AddCargoUnit(cargoUnitData.position, cargoUnitData.rotation);
                    }
                }
                //cargoCountableDataToLoad.Add(wdata.cargoCountable);
            }
            train.OnReady();

            buyMenu.ClearLots();
            foreach(LotData lotData in SaveData.current.buyMenuData.lots)
            {
                buyMenu.CreateLot(WagonFactory.Instance.GetWagonConfig(lotData.wagonType), lotData.price);
            }

            tutorialManager.startOver = false;
            for (int i = 0; i < tutorialManager.transform.childCount; i++)
            {
                tutorialManager.transform.GetChild(i).gameObject.SetActive(SaveData.current.tutorialData.eventTriggers[i]);
            }
        }


        /*private void OnActiveSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            //check if the scene is active scene (field) and move everything according loading here
            if (scene == SceneManager.GetActiveScene())
            {
                SceneManager.sceneLoaded -= OnActiveSceneLoaded;
                
            }
        }*/

        public bool SaveFileExists()
        {
            return File.Exists(Application.persistentDataPath + "/saves/" + saveFileName + ".save");
        }

        public void DeleteSaveFile()
        {
            File.Delete(Application.persistentDataPath + "/saves/" + saveFileName + ".save");
        }

        public void OnTrainCrash()
        {
            StartCoroutine(WaitAndLoad());
        }

        private IEnumerator WaitAndLoad(float seconds = 3f)
        {
            yield return new WaitForSeconds(seconds);
            OnLoad();
            yield return null;
        }

    } 
}
