﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestSceneLoadCallback : MonoBehaviour
{
    private void OnEnable()
    {
        Debug.Log("subscribing");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        Debug.Log("UNsubscribing");
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("CALLBACK: scene " + scene.name + " was loaded");
        //SceneManager.MoveGameObjectToScene(gameObject, scene);
    }

    public void TestMessage(string s)
    {
        Debug.Log(s);
    }
}
