﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    public class CargoLossArea : MonoBehaviour
    {
        public event Action<GameObject> OnCargoUnitLoss = delegate { };

        private void OnTriggerEnter(Collider other)
        {
            OnCargoUnitLoss?.Invoke(other.gameObject);
        }
    } 
}
