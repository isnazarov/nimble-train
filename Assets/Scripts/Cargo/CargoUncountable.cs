﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{

    [RequireComponent(typeof(Wagon))]
    public class CargoUncountable : Cargo
    {
        //[SerializeField] private GameObject cargoUnitPrefab;
        //[SerializeField] private Transform cargoLoader;
        [SerializeField, Range(0f, 1000f)] private float maxCapacity;
        [SerializeField] private CargoSurface cargoSurface;
        [SerializeField, Range(0.001f, 1f)] private float lossSpeedFactor; // amount of cargo loss per second with tilt angle is 90 deg (0 to 1)
        [SerializeField] private ParticleSystem cargoGenerator;

        //[SerializeField] private CargoConfig _cargoConfig;
        private int _cost;

        public float CurrentHeight
        {
            get
            {
                return cargoSurface.CurrentHeight;
            }
        }

        private List<GameObject> cargo = new List<GameObject>();
        private Wagon wagon;
        private float lossTimer = 0f;
        private float cargoLossCounter = 0f;
        private float _currentCapacity = 0f;
        public float CurrentCapacity => _currentCapacity;

        private void Awake()
        {
            wagon = GetComponent<Wagon>();
        }

        private void Start()
        {
            //currentCapacity = maxCapacity / 2;
            //_currentCapacity = 0f;
            if (cargoSurface != null)
            {
                cargoSurface.SetHeight(_currentCapacity / maxCapacity);
                cargoSurface.SetMaterial(_cargoConfig.surfaceMaterial);
            }
            if (cargoGenerator != null)
            {
                cargoGenerator.GetComponent<ParticleSystemRenderer>().material = _cargoConfig.surfaceMaterial;
            }
            _cost = _cargoConfig.cost;
        }

        private void Update()
        {
            /*if (Input.GetKeyDown(KeyCode.L))
            {
                Load();
            }*/
            LoseCargo();
        }

        private void LoseCargo()
        {

            float lossSpeed = Mathf.Lerp(0f, lossSpeedFactor, Mathf.Abs(wagon.TiltAngleSigned) / 90) * _currentCapacity;

            // Particle modules are structures that keep reference to their particle system instances and transfer all changes to that instances
            // Changing something in copies of that structs also make changes in corresponding particle systems.
            // So these modules are actually interfaces to particle systems 
            // (https://blogs.unity3d.com/2016/04/20/particle-system-modules-faq/?_ga=2.214949716.2132149246.1570712614-224446019.1514378245)
            var emissionModule = cargoGenerator.emission;
            emissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(lossSpeed);

            if (lossSpeed > 0.1) // prevent loss when tilt angle is close to zero 
            {
                _currentCapacity -= lossSpeed * Time.deltaTime;
                cargoSurface.SetHeight(_currentCapacity / maxCapacity);
                //TODO: create event when cargo is over
            }

        }

        public void SetCapacity(float capacity)
        {
            _currentCapacity = capacity;
            cargoSurface.SetHeight(_currentCapacity / maxCapacity);
        }

        public override void Load()
        {
            StartCoroutine(LoadingProcess());
        }

        private IEnumerator LoadingProcess(float fullLoadTime = 3f)
        {
            while (_currentCapacity < maxCapacity)
            {
                _currentCapacity += maxCapacity * Time.deltaTime / fullLoadTime;
                cargoSurface.SetHeight(_currentCapacity / maxCapacity);
                yield return null;
            }
            _currentCapacity = maxCapacity;
            OnLoadFinished();
            yield return null;
        }

        public override void Unload()
        {
            StartCoroutine(UnloadingProcess());
        }

        private IEnumerator UnloadingProcess(float fullLoadTime = 3f)
        {
            float cargoIntCounter = 0;
            while (_currentCapacity > 0)
            {
                float dCargo = maxCapacity * Time.deltaTime / fullLoadTime;
                _currentCapacity -= dCargo;
                cargoIntCounter += dCargo;
                if (cargoIntCounter >= 1f)
                {
                    GameSession.Instance.AddMoney((int)cargoIntCounter * _cost);
                    GenerateCargoIcon();
                    for (int i = 0; i < (int)cargoIntCounter; i++)
                    {
                        GameSession.Instance.AddCargo(Type);
                    }
                    cargoIntCounter -= (int)cargoIntCounter;
                }
                cargoSurface.SetHeight(_currentCapacity / maxCapacity);
                yield return null;
            }
            _currentCapacity = 0;
            OnLoadFinished();
            yield return null;
        }

    }

}