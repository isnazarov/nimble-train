﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace NimbleTrain
{

    [RequireComponent(typeof(Wagon))]
    public class CargoCountable : Cargo
    {
        // we subscribe to the event in the loss area. When the trigger fires the event runs with the cargo unit object as an argument.
        [SerializeField] private CargoLossArea cargoLossArea;
        [SerializeField] private Transform cargoLoader;
        [SerializeField, Range(1f, 100f)] private int maxCapacity;

        public List<Rigidbody> cargoUnits;

        //[SerializeField] private CargoConfig _cargoConfig;
        private int _cost;

        private void Awake()
        {
            cargoLossArea.OnCargoUnitLoss += LoseCargoUnit;
            cargoUnits = new List<Rigidbody>();
            _cost = _cargoConfig.cost;
            InitializeLoadedCargo();
        }
        
        private void InitializeLoadedCargo()
        {
            bool isInitiallyLoaded = false;
            foreach (Transform cargoTransform in cargoLoader)
            {
                Rigidbody cargoRB = cargoTransform.GetComponent<Rigidbody>();
                if (cargoRB != null)
                {
                    isInitiallyLoaded = true;
                    cargoUnits.Add(cargoRB);
                }
            }
            if (isInitiallyLoaded)
            {
                StartCoroutine(PlaceInitialCargo());
            }
        }

        private IEnumerator PlaceInitialCargo()
        {
            //Vector3 cargoLoaderInitialPos = cargoLoader.position;
            List<Vector3> cargoUnitsLocalPositions = new List<Vector3>();
            List<Quaternion> cargoUnitsLocalRotations = new List<Quaternion>();
            foreach (Rigidbody cargoRB in cargoUnits)
            {
                cargoUnitsLocalPositions.Add(cargoRB.transform.localPosition);
                cargoUnitsLocalRotations.Add(cargoRB.transform.localRotation);
            }
            yield return null;
            for (int i = 0; i < cargoUnits.Count; i++)
            {
                cargoUnits[i].transform.localPosition = cargoUnitsLocalPositions[i];
                cargoUnits[i].transform.localRotation = cargoUnitsLocalRotations[i];
            }
            yield return null;
        }

        private void LoseCargoUnit(GameObject cargoUnit)
        {
            Rigidbody rb = cargoUnit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                if (cargoUnits.Remove(rb)) // if false then this is not cargo from our wagon
                {
                    cargoUnit.layer = 0;
                    Destroy(cargoUnit, 3f);
                }
            }
        }

        public void AddCargoUnit(Vector3 position, Quaternion rotation)
        {
            Rigidbody cargoUnit = Instantiate(_cargoConfig.cargoUnitPrefab, position, rotation);
            cargoUnit.transform.SetParent(cargoLoader);
            cargoUnits.Add(cargoUnit);
        }

        public override void Load()
        {
            StartCoroutine(LoadingProcess());
        }

        private IEnumerator LoadingProcess(float loadingPeriod = 0.5f)
        {
            while (cargoUnits.Count < maxCapacity)
            {
                Rigidbody cargoUnit = Instantiate(_cargoConfig.cargoUnitPrefab, cargoLoader);
                cargoUnits.Add(cargoUnit);
                yield return new WaitForSeconds(loadingPeriod);
            }
            OnLoadFinished();
            yield return null;
        }

        public override void Unload()
        {
            StartCoroutine(UnloadingProcess());
        }

        private IEnumerator UnloadingProcess(float loadingPeriod = 0.3f)
        {
            foreach (Rigidbody cargoUnit in cargoUnits)
            {
                GameSession.Instance.AddMoney(_cost);
                GameSession.Instance.AddCargo(Type);
                GenerateCargoIcon();
                Destroy(cargoUnit.gameObject);
                yield return new WaitForSeconds(loadingPeriod);
            }
            cargoUnits.Clear();
            OnLoadFinished();
            yield return null;
        }

        public void StopImmediately()
        {
            foreach (Rigidbody rb in cargoUnits)
            {
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
    }

}