﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    [Serializable]
    public class CargoTask
    {
        [SerializeField] private CargoType _cargo;
        [SerializeField] private bool _isAccepted = false;
        [SerializeField] private bool _isOffered = false;
        [SerializeField] private int _quantity = 0;

        public CargoType Cargo => _cargo;
        public bool IsAccepted => _isAccepted;
        public bool IsOffered => _isOffered;
        public int Quantity => _quantity;

        public CargoTask(CargoType cargo, bool isAccepted, bool isOffered, int quantity)
        {
            _cargo = cargo;
            _isAccepted = isAccepted;
            _isOffered = isOffered;
            _quantity = quantity <= 0 ? 0 : quantity;
        }
    }
}
