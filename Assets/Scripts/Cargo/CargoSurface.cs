﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [RequireComponent(typeof(MeshFilter))]
    public class CargoSurface : MonoBehaviour
    {
        [SerializeField, Range(1, 50)] private int xResolution;
        [SerializeField, Range(1, 50)] private int zResolution;
        [SerializeField] private Transform limit1;
        [SerializeField] private Transform limit2;

        public float CurrentHeight { get { return transform.localPosition.y; } }

        private float _maxHeight;
        private float _minHeight;
        private Mesh _mesh;
        private float _width;
        private float _length;

        private float currentHeight;
        private float currentDir = 0.1f;

        private void Awake()
        {
            _mesh = GetComponent<MeshFilter>().mesh;
            SetDimensions();
            GenerateMesh();
        }

        private void Start()
        {
            //currentHeight = 0.5f;
        }

        public void SetMaterial(Material material)
        {
            GetComponent<Renderer>().material = material;
        }

        private void SetDimensions()
        {
            _maxHeight = Mathf.Max(limit1.localPosition.y, limit2.localPosition.y);
            _minHeight = Mathf.Min(limit1.localPosition.y, limit2.localPosition.y);
            _width = Mathf.Abs(limit1.localPosition.x - limit2.localPosition.x);
            _length = Mathf.Abs(limit1.localPosition.z - limit2.localPosition.z);
        }

        /*private void Update()
        {
            if(Input.GetKey(KeyCode.M))
            {
                UpdateMesh();
            }

            currentHeight += currentDir * Time.deltaTime;
            if (currentHeight >= 1f)
            {
                currentHeight = 1f;
                currentDir *= -1;
            }
            if (currentHeight <= 0f)
            {
                currentHeight = 0f;
                currentDir *= -1;
            }
            SetHeight(currentHeight);

        }*/

        public void SetHeight(float t)
        {
            t = Mathf.Clamp01(t);
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(_minHeight, _maxHeight, t), transform.localPosition.z);
        }

        private void UpdateMesh()
        {
            //Vector3[] vertices = new Vector3[_mesh.vertices.Length];
            Vector3[] vertices = _mesh.vertices;
            Vector2[] uvs = _mesh.uv;
            for (int i = 0; i < vertices.Length; i++)
            {
                //vertices[i] = vertices[i] + Vector3.up * Random.Range(-0.2f, 0.2f);
                vertices[i] = new Vector3(vertices[i].x, Random.Range(-0.2f, 0.2f), vertices[i].z);
                uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
            }
            _mesh.vertices = vertices;
            _mesh.uv = uvs;
            _mesh.RecalculateNormals();
            /*Mesh m = new Mesh();
            m.vertices = vertices;
            m.RecalculateNormals();
            GetComponent<MeshFilter>().sharedMesh = m;*/
        }

        private void GenerateMesh()
        {
            //var mesh = new Mesh();
            int vertCount = (xResolution + 1) * (zResolution + 1);
            int[] triangleIndices = new int[xResolution * zResolution * 6];
            Vector3[] vertices = new Vector3[vertCount];
            Vector2[] uvs = new Vector2[vertCount];

            float xStep = _width / xResolution;
            float zStep = _length / zResolution;
            float xBase = -_width / 2;
            float zBase = -_length / 2;

            int vertIndex = 0;
            float x = xBase;
            float z = zBase;
            for (int i = 0; i <= zResolution; i++)
            {
                for (int j = 0; j <= xResolution; j++)
                {
                    vertices[vertIndex] = new Vector3(x, Random.Range(-0.2f, 0.2f), z);
                    uvs[vertIndex] = new Vector2(x, z);
                    vertIndex++;
                    x += xStep;
                }
                z += zStep;
                x = xBase;
            }

            int triIndex = 0;
            for (int i = 0; i < zResolution; i++)
            {
                for (int j = 0; j < xResolution; j++)
                {
                    int a = (xResolution + 1) * i + j;
                    int b = a + 1;
                    int c = a + xResolution + 1;
                    int d = c + 1;
                    triangleIndices[triIndex] = a; triIndex++;
                    triangleIndices[triIndex] = c; triIndex++;
                    triangleIndices[triIndex] = b; triIndex++;
                    triangleIndices[triIndex] = c; triIndex++;
                    triangleIndices[triIndex] = d; triIndex++;
                    triangleIndices[triIndex] = b; triIndex++;
                }
            }
            //GetComponent<MeshFilter>().sharedMesh.Clear();
            _mesh.vertices = vertices;
            _mesh.triangles = triangleIndices;
            _mesh.uv = uvs;
            _mesh.RecalculateNormals();
            //_mesh = mesh;

            //GetComponent<MeshFilter>().sharedMesh = mesh;
        }

    }

}