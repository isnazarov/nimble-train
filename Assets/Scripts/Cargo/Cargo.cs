﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

namespace NimbleTrain
{
    public abstract class Cargo : MonoBehaviour
    {
        [SerializeField] protected CargoConfig _cargoConfig;
        public CargoType Type => _cargoConfig.cargoType;

        public abstract void Load();
        public abstract void Unload();
        public event Action OnLoaded = delegate { };

        // events cannot be called directly from child classes! So we use separate method in base class.
        protected void OnLoadFinished()
        {
            OnLoaded?.Invoke();
        }

        protected void GenerateCargoIcon()
        {
            CargoIconGenerator.Instance.Generate(_cargoConfig, GetComponent<Wagon>().Platform.transform.position);
        }
    } 
}
