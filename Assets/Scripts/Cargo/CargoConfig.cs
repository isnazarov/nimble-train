﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [CreateAssetMenu(fileName = "Cargo/NewCargoConfig", menuName = "Cargo")]
    public class CargoConfig : ScriptableObject
    {
        //public ParticleSystem cargoGenerator;
        public CargoType cargoType;
        public int cost;
        public Material surfaceMaterial; // for uncountable
        public Rigidbody cargoUnitPrefab; // for countable
        public Sprite cargoIcon;
    } 
}
