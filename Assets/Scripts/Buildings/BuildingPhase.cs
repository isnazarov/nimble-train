﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BuildingPhase : RuntimeSet<Buildable>
{
    public void Activate()
    {
        foreach (Buildable b in Items)
        {
            b.Activate();
        }
    }

    public void SetAsActive()
    {
        foreach (Buildable b in Items)
        {
            b.Activate(true);
        }
    }

    public void SetAsInactive()
    {
        foreach (Buildable b in Items)
        {
            b.Rollback(true);
        }
    }
}
