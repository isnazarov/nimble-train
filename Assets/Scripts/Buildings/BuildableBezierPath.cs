﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using UnityEngine.U2D;

namespace NimbleTrain
{
    public class BuildableBezierPath : Buildable
    {
        //private PathCreator _pathCreator;
        private PathCreatorSpriteShapeGenerator _shapeGenerator;

        private PathCreatorSpriteShapeGenerator ShapeGenerator
        {
            get
            {
                if (_shapeGenerator == null)
                {
                    _shapeGenerator = GetComponent<PathCreatorSpriteShapeGenerator>();
                    if (_shapeGenerator == null)
                        throw new System.NullReferenceException("Could not get PathCreatorSpriteShapeGenerator component");
                }
                return _shapeGenerator;
            }
        }
        //private Buildable _railsShape;
        //private Buildable _sleepersShape;

        protected override void Awake()
        {
            /*_pathCreator = GetComponent<PathCreator>();
            if (_pathCreator == null)
                throw new System.NullReferenceException("Could not get PathCreator component");
            */

            /*_shapeGenerator = GetComponent<PathCreatorSpriteShapeGenerator>();
            if (_shapeGenerator == null)
                throw new System.NullReferenceException("Could not get PathCreatorSpriteShapeGenerator component");
            */
            base.Awake();
        }

        /*protected override void SetVisible(bool visible = true)
        {
            if (ShapeGenerator.RailsShape != null)
                ShapeGenerator.RailsShape.gameObject.SetActive(visible);
            if (ShapeGenerator.SleepersShape != null)
                ShapeGenerator.SleepersShape.gameObject.SetActive(visible);

            //_pathCreator.enabled = visible;
        }*/

        public override void Build()
        {
            if (!_inProcess && !_isBuilt)
            {
                StartCoroutine(RailwayBuildProcess());
                //StartCoroutine(BuildingProcess(_shapeGenerator.SleepersShape.transform));
                _isBuilt = true;
                Initialize();
            }
        }

        public override void Remove()
        {
            if (!_inProcess && _isBuilt)
            {
                StartCoroutine(RailwayBuildProcess(true));
                //StartCoroutine(BuildingProcess(_shapeGenerator.SleepersShape.transform, true));
                _isBuilt = false;
                Initialize();
            }
        }

        private IEnumerator RailwayBuildProcess(bool remove = false)
        {
            /*if (!remove)
            {
                SetVisible();
            }*/

            if (remove)
                ShapeGenerator.RailsShape.GetComponent<Buildable>().Remove();
                //StartCoroutine(BuildingProcess(_shapeGenerator.RailsShape.transform, remove));
            else
                ShapeGenerator.SleepersShape.GetComponent<Buildable>().Build();
                //StartCoroutine(BuildingProcess(_shapeGenerator.SleepersShape.transform));

            yield return new WaitForSeconds(1f);

            if (remove)
                ShapeGenerator.SleepersShape.GetComponent<Buildable>().Remove();
                //StartCoroutine(BuildingProcess(_shapeGenerator.SleepersShape.transform, remove));
            else
                ShapeGenerator.RailsShape.GetComponent<Buildable>().Build();
                //StartCoroutine(BuildingProcess(_shapeGenerator.RailsShape.transform));

            /*if (remove)
            {
                yield return new WaitForSeconds(1f);
                SetVisible(false);
            }*/
                
            yield return null;
        }



        public override void SetAsBuilt(bool remove = false)
        {
            /*if (!remove)
            {
                SetVisible();
            }*/

            if (remove)
                ShapeGenerator.RailsShape.GetComponent<Buildable>().SetAsRemoved();
            else
                ShapeGenerator.SleepersShape.GetComponent<Buildable>().SetAsBuilt();

            if (remove)
                ShapeGenerator.SleepersShape.GetComponent<Buildable>().SetAsRemoved();
            else
                ShapeGenerator.RailsShape.GetComponent<Buildable>().SetAsBuilt();

            Initialize();
            /*if (remove)
            {
                SetVisible(false);
            }*/
        }

        private void Initialize()
        {
            PathConnector connector = GetComponent<PathConnector>();
            if (connector != null)
            {
                //connector.Initialize();
                foreach (Connection conn in connector.connections)
                {
                    Path otherPath = conn.GetOtherPath(GetComponent<Path>());
                    if (otherPath != null)
                    {
                        PathConnector pathConnector = otherPath.GetComponent<PathConnector>();
                        if (pathConnector != null)
                        {
                            pathConnector.Initialize();
                            if (otherPath == PathManager.Instance.CurrentPath)
                            {
                                PathManager.Instance.ActivateSwitchLever();
                            }
                        }
                    }
                }
            }
        }

    }

}