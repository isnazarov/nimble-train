﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Buildable : MonoBehaviour
{
    [SerializeField] protected float minY;
    [SerializeField] protected float maxY;
    [SerializeField] protected FloatReference time;
    [SerializeField] protected bool isRising;
    [SerializeField] protected List<ParticleSystem> particles = new List<ParticleSystem>();

    [SerializeField] private BuildingPhase buildingPhase;
    [SerializeField] protected bool toRemove;
    public bool ToRemove => toRemove;
    public float Duration => time.Value;

    protected bool _isBuilt;
    protected bool _inProcess = false;

    public event Action<Buildable> OnBuildingProcessFinished = delegate { };

    protected virtual void Awake()
    {
        if (maxY < minY)
        {
            maxY = minY;
        }
    }

    
    private void OnEnable()
    {
        _isBuilt = ToRemove;
        //SetVisible(ToRemove);
        if (buildingPhase != null)
        {
            buildingPhase.Add(this);
        }
    }

    private void OnDisable()
    {
        if (buildingPhase != null)
        {
            buildingPhase.Remove(this);
        }
    }

    protected virtual void ProcessComplete()
    {
        OnBuildingProcessFinished?.Invoke(this);
    }

    /*protected virtual void SetVisible(bool visible = true)
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer != null) meshRenderer.enabled = visible;
    }*/

    public virtual void Activate(bool quickly = false)
    {
        if (!quickly)
        {
            if (toRemove)
            {
                Remove();
            }
            else
            {
                Build();
            }
        }
        else
        {
            if (toRemove)
            {
                SetAsRemoved();
            }
            else
            {
                SetAsBuilt();
            }
        }
    }

    public virtual void Rollback(bool quickly = false)
    {
        if (!quickly)
        {
            if (toRemove)
            {
                Build();
            }
            else
            {
                Remove();
            }
        }
        else
        {
            if (toRemove)
            {
                SetAsBuilt();
            }
            else
            {
                SetAsRemoved();
            }
        }
    }

    public virtual void Build()
    {
        if (!_inProcess && !_isBuilt)
        {
            StartCoroutine(BuildingProcess(transform));
            _isBuilt = true;
        }
        ProcessComplete();
    }

    public virtual void Remove()
    {
        if (!_inProcess && _isBuilt)
        {
            StartCoroutine(BuildingProcess(transform, true));
            _isBuilt = false;
        }
        ProcessComplete();
    }

    protected virtual IEnumerator BuildingProcess(Transform t, bool remove = false)
    {
        _inProcess = true;
        float timer = time.Value;

        if (isRising)
            ShowParticles(time.Value);

        //while (timer > 0f)
        //{
            if ((isRising && !remove) || (!isRising && remove))
            {
                while (timer > 0f)
                {
                    t.localPosition = new Vector3(t.localPosition.x, Mathf.Lerp(minY, maxY, 1 - timer / time.Value), t.localPosition.z);
                    timer -= Time.deltaTime;
                    yield return null;
                }
                t.localPosition = new Vector3(t.localPosition.x, maxY, t.localPosition.z);
            }
            else
            {
                while (timer > 0f)
                {
                    t.localPosition = new Vector3(t.localPosition.x, Mathf.Lerp(maxY, minY, 1 - timer / time.Value), t.localPosition.z);
                    timer -= Time.deltaTime;
                    yield return null;
                }
                t.localPosition = new Vector3(t.localPosition.x, minY, t.localPosition.z);
            }
            //timer -= Time.deltaTime;
            //yield return null;
        //}

        if (!isRising && !remove)
            ShowParticles(1f);

        _inProcess = false;
        yield return null;
    }


    public virtual void SetAsBuilt(bool remove = false)
    {
        if (_inProcess)
        {
            StopAllCoroutines();
            _inProcess = false;
        }
        if ((isRising && !remove) || (!isRising && remove))
        {
            transform.localPosition = new Vector3(transform.localPosition.x, maxY, transform.localPosition.z);
        }
        else
        {
            transform.localPosition = new Vector3(transform.localPosition.x, minY, transform.localPosition.z);
        }
        _isBuilt = !remove;
    }

    public virtual void SetAsRemoved()
    {
        SetAsBuilt(true);
    }

    public virtual void ShowParticles(float duration = 5f)
    {
        /*foreach (ParticleSystem p in particles)
        {
            var main = p.main;
            p.Stop();
            main.duration = duration;
        }*/
        foreach (ParticleSystem p in particles)
        {
            p.Play();
        }
    }
}
