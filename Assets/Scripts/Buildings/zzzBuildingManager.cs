﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    // It can easily be SO instance!

    [SerializeField] private List<BuildingPhase> phases = new List<BuildingPhase>();
    [SerializeField] private int currentPhase = -1; // for debug
    private int _nextPhase = 0;

    /*private void Update()
    {
        if (Input.GetKey(KeyCode.P))
        {
            ActivateNextPhase();
        }
    }*/

    public void Initialize()
    {
        for (int i = 0; i < phases.Count; i++)
        {
            if (phases[i] != null)
            {
                if (i < _nextPhase)
                {
                    phases[i].SetAsActive();
                }
                else
                {
                    phases[i].SetAsInactive();
                }
            }
        }
    }

    private void ActivatePhase (int phaseIndex)
    {
        if (phases[phaseIndex] != null)
        {
            phases[phaseIndex].Activate();
        }
    }

    public void ActivateNextPhase()
    {
        ActivatePhase(_nextPhase);
        _nextPhase++;
    }

    
}
