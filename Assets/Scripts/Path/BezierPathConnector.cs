﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PathCreation.Examples;
using PathCreation;
using System;

// Control mode should not be Automatic when connecting paths!
// To connect this path just drag n drop other path to corresponding field in inspector, then deselect/select this path

namespace NimbleTrain
{
    public class BezierPathConnector : PathConnector
    {
        protected override void AutoConnectToPath(Path otherPath)
        {
            // find closest distance between 4 pairs of points 
            // connect this path's endpoint to closest point of the other path
            PathCreator otherPathCreator = otherPath.GetComponent<PathCreator>();
            if (otherPathCreator == null)
            {
                throw new ArgumentException("otherPath doesn't contain PathCreator component");
            }
            pathCreator.bezierPath.ControlPointMode = BezierPath.ControlMode.Aligned;
            otherPathCreator.bezierPath.ControlPointMode = BezierPath.ControlMode.Aligned;

            Vector3 otherPathLastAnchorGlobalPosition = otherPathCreator.bezierPath.GetPoint(otherPathCreator.bezierPath.NumPoints - 1) + otherPath.transform.position;
            Vector3 otherPathFirstAnchorGlobalPosition = otherPathCreator.bezierPath.GetPoint(0) + otherPath.transform.position;

            float[] sqrDistances = new float[4] {
                (pathCreator.bezierPath.GetPoint(0) + transform.position - otherPathFirstAnchorGlobalPosition).sqrMagnitude,
                (pathCreator.bezierPath.GetPoint(0) + transform.position - otherPathLastAnchorGlobalPosition).sqrMagnitude,
                (pathCreator.bezierPath.GetPoint(pathCreator.bezierPath.NumPoints - 1) + transform.position - otherPathFirstAnchorGlobalPosition).sqrMagnitude,
                (pathCreator.bezierPath.GetPoint(pathCreator.bezierPath.NumPoints - 1) + transform.position  - otherPathLastAnchorGlobalPosition).sqrMagnitude
            };

            int minIndex = 0;
            for (int i = 1; i < 4; i++)
            {
                if(sqrDistances[i] < sqrDistances[minIndex])
                {
                    minIndex = i;
                }
            }

            switch (minIndex)
            {
                case 0:
                    Adapt(this.pathCreator, 0, otherPathCreator, 0);
                    break;
                case 1:
                    Adapt(this.pathCreator, 0, otherPathCreator, 1);
                    break;
                case 2:
                    Adapt(this.pathCreator, 1, otherPathCreator, 0);
                    break;
                case 3:
                    Adapt(this.pathCreator, 1, otherPathCreator, 1);
                    break;
                default:
                    throw new IndexOutOfRangeException("minIndex value: " + minIndex + " but it should be in range 0-3");
            }
        }

        private void Adapt (PathCreator path, int pathPoint, PathCreator otherPath,  int otherPathPoint)
        {
            int pathAnchorIndex, pathControlIndex, pathNearestAnchorIndex, otherPathAnchorIndex, otherPathControlIndex;
            if (pathPoint != 0)
            {
                pathPoint = 1;
                pathAnchorIndex = path.bezierPath.NumPoints - 1;
                pathControlIndex = pathAnchorIndex - 1;
                pathNearestAnchorIndex = pathAnchorIndex - 3;
            }
            else
            {
                pathAnchorIndex = 0;
                pathControlIndex = 1;
                pathNearestAnchorIndex = 3;
            }
                
            if (otherPathPoint != 0)
            {
                otherPathPoint = 1;
                otherPathAnchorIndex = otherPath.bezierPath.NumPoints - 1;
                otherPathControlIndex = otherPathAnchorIndex - 1;
            }
            else
            {
                otherPathAnchorIndex = 0;
                otherPathControlIndex = 1;
            }

            Vector3 otherPathAnchorGlobalPosition = otherPath.bezierPath.GetPoint(otherPathAnchorIndex) + otherPath.transform.position;
            path.bezierPath.MovePoint(pathAnchorIndex, otherPathAnchorGlobalPosition - path.transform.position, true);

            Vector3 otherPathControlGlobalPosition = otherPath.bezierPath.GetPoint(otherPathControlIndex) + otherPath.transform.position;
            Vector3 otherPathControlMirroredOffset = otherPathAnchorGlobalPosition - otherPathControlGlobalPosition;
            float newDistanceBetweenAnchors = (path.bezierPath.GetPoint(pathNearestAnchorIndex) - path.bezierPath.GetPoint(pathAnchorIndex)).magnitude;
            Vector3 pathControlOffset = otherPathControlMirroredOffset.normalized * newDistanceBetweenAnchors * .5f;
            pathCreator.bezierPath.MovePoint(pathControlIndex, pathCreator.bezierPath.GetPoint(pathAnchorIndex) + pathControlOffset, true);

            Connection connection = new Connection(path.GetComponent<BezierPathAdapter>(), pathPoint, otherPath.GetComponent<BezierPathAdapter>(), otherPathPoint);
            AddConnection(connection);
        }

        protected override void AdaptToPreviousPath()
        {
            Adapt(pathCreator, 0, previousPath.GetComponent<PathCreator>(), 1);
        }

        protected override void AdaptToNextPath()
        {
            Adapt(pathCreator, 1, previousPath.GetComponent<PathCreator>(), 0);
        }
    }

}