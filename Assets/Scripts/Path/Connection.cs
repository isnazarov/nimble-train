﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    [Serializable]
    public class Connection : IEquatable<Connection>
    {
        [SerializeField]
        private Path path0;
        [SerializeField]
        private Path path1;
        [SerializeField]
        private int path0ConnectionPoint; // 0 - start, 1 (cast from any non-zero number) - end 
        [SerializeField]
        private int path1ConnectionPoint;

        // Workaround needed because serialized Path fields allow null values
        public bool IsConsistent
        {
            get
            {
                if ((this.path0 == null) || (this.path1 == null))
                {
                    return false;
                }
                if (!this.path0.gameObject.activeInHierarchy || !this.path1.gameObject.activeInHierarchy)
                {
                    return false;
                }
                return true;
            }
        }

        public Connection(Path path0, int path0Point, Path path1, int path1Point)
        {
            if (path0 == null)
            {
                throw new System.ArgumentNullException("path0");
            }
            if (path1 == null)
            {
                throw new System.ArgumentNullException("path1");
            }
            if (path0 == path1)
            {
                throw new System.ArgumentException("path0 and path1 can not be the same path");
            }
            this.path0 = path0;
            this.path1 = path1;
            this.path0ConnectionPoint = path0Point == 0 ? 0 : 1;
            this.path1ConnectionPoint = path1Point == 0 ? 0 : 1;
        }

        public bool Contains(Path p)
        {
            if ((path0 == p) || (path1 == p))
            {
                return true;
            }
            return false;
        }
        
        public bool Equals(Connection other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }
            if (
                (this.path0 == other.path0
                && this.path1 == other.path1
                && this.path0ConnectionPoint == other.path0ConnectionPoint
                && this.path1ConnectionPoint == other.path1ConnectionPoint)
                ||
                (this.path0 == other.path1
                && this.path1 == other.path0
                && this.path0ConnectionPoint == other.path1ConnectionPoint
                && this.path1ConnectionPoint == other.path0ConnectionPoint)
                )
            {
                return true;
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Connection);
        }

        public override string ToString()
        {
            return path0.gameObject.name + "; " + path0ConnectionPoint.ToString() + " <=> " + path1ConnectionPoint.ToString() + "; " + path1.gameObject.name;
        }

        public override int GetHashCode()
        {
            long hash = 0;
            if (path0.gameObject.GetInstanceID() < path1.gameObject.GetInstanceID())
            { 
                hash = (path0.gameObject.GetInstanceID() * 0x00010000) ^ (path0ConnectionPoint & 0x80000000)
                    | (path1.gameObject.GetInstanceID() ^ (path1ConnectionPoint & 0x00008000));
            }
            else
            {
                hash = (path1.gameObject.GetInstanceID() * 0x00010000) ^ (path1ConnectionPoint & 0x80000000)
                    | (path0.gameObject.GetInstanceID() ^ (path0ConnectionPoint & 0x00008000));
            }
            return (int)hash;
        }

        public static bool operator ==(Connection c1, Connection c2)
        {
            if (object.ReferenceEquals(c1, null))
            {
                if (object.ReferenceEquals(c2, null))
                {
                    // null == null = true.
                    return true;
                }
                return false;
            }
            // Equals handles case of null on right side.
            return c1.Equals(c2);
        }

        public static bool operator !=(Connection c1, Connection c2)
        {
            return !(c1 == c2);
        }

        public Path GetOtherPath(Path thisPath)
        {
            if (path0 == thisPath)
            {
                return path1;
            }
            if (path1 == thisPath)
            {
                return path0;
            }
            return null;
        }

        public int GetConnectionPoint(Path path)
        {
            if (path0 == path)
            {
                return path0ConnectionPoint;
            }
            if (path1 == path)
            {
                return path1ConnectionPoint;
            }
            return -1;
        }
    }
/*
 * // This version cannot be serialized becaude of array of non-primitives
    [Serializable]
    public class Connection : IEquatable<Connection>
    {
        private Path[] paths = new Path[2];
        private int[] pathsConnectionPoint = new int[2]; // 0 - start, 1 (cast from any non-zero number) - end 

        public Connection(Path path0, int path0Point, Path path1, int path1Point)
        {
            if (path0 == null)
            {
                throw new System.ArgumentNullException("path0");
            }
            if (path1 == null)
            {
                throw new System.ArgumentNullException("path1");
            }
            if (path0 == path1)
            {
                throw new System.ArgumentException("path0 and path1 can not be the same path");
            }
            this.paths[0] = path0;
            this.paths[1] = path1;
            this.pathsConnectionPoint[0] = path0Point == 0 ? 0 : 1;
            this.pathsConnectionPoint[1] = path1Point == 0 ? 0 : 1;
        }

        public bool Contains(Path p)
        {
            if ((paths[0] == p) || (paths[1] == p))
            {
                return true;
            }
            return false;
        }

        public bool Equals(Connection other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }
            if (
                (this.paths[0] == other.paths[0]
                && this.paths[1] == other.paths[1]
                && this.pathsConnectionPoint[0] == other.pathsConnectionPoint[0]
                && this.pathsConnectionPoint[1] == other.pathsConnectionPoint[1])
                ||
                (this.paths[0] == other.paths[1]
                && this.paths[1] == other.paths[0]
                && this.pathsConnectionPoint[0] == other.pathsConnectionPoint[1]
                && this.pathsConnectionPoint[1] == other.pathsConnectionPoint[0])
                )
            {
                return true;
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Connection);
        }

        public override int GetHashCode()
        {
            long hash = 0;
            if (paths[0].gameObject.GetInstanceID() < paths[1].gameObject.GetInstanceID())
            {
                hash = (paths[0].gameObject.GetInstanceID() * 0x00010000) ^ (pathsConnectionPoint[0] & 0x80000000)
                    | (paths[1].gameObject.GetInstanceID() ^ (pathsConnectionPoint[1] & 0x00008000));
            }
            else
            {
                hash = (paths[1].gameObject.GetInstanceID() * 0x00010000) ^ (pathsConnectionPoint[1] & 0x80000000)
                    | (paths[0].gameObject.GetInstanceID() ^ (pathsConnectionPoint[0] & 0x00008000));
            }
            return (int)hash;
        }

        public static bool operator ==(Connection c1, Connection c2)
        {
            if (object.ReferenceEquals(c1, null))
            {
                if (object.ReferenceEquals(c2, null))
                {
                    // null == null = true.
                    return true;
                }
                return false;
            }
            // Equals handles case of null on right side.
            return c1.Equals(c2);
        }

        public static bool operator !=(Connection c1, Connection c2)
        {
            return !(c1 == c2);
        }
    }*/
}
