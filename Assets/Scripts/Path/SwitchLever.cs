﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    //[ExecuteInEditMode]
    public class SwitchLever : MonoBehaviour
    {
        [SerializeField] private Transform leverBase;
        private Direction currentDirection;

        public event Action<Direction> OnToggle = delegate { };

        public void Toggle()
        {
            if (currentDirection == Direction.left)
            {
                currentDirection = Direction.right;
            }
            else
            {
                currentDirection = Direction.left;
            }
            Toggle(currentDirection);
        }

        public void Toggle(Direction direction)
        {
            if (direction == Direction.left)
            {
                leverBase.localRotation = Quaternion.Euler(0f, 0f, 45f);
            }
            else
            {
                leverBase.localRotation = Quaternion.Euler(0f, 0f, -45f);
            }
            OnToggle?.Invoke(direction);
        }
        
    } 
}
