﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace NimbleTrain
{
    public abstract class Path : MonoBehaviour
    {
        public virtual Path NextPath { get; }
        public virtual Path PreviousPath { get; }
        public virtual PathConnector Connector { get; }

        public abstract int WpCount { get; }
        public abstract bool isLooped { get; }

        public abstract event Action onUpdate;

        public abstract Vector3 GetPosition(float distance);
        public abstract Vector3 GetPositionAtPathUnit(float unit); // where integer part of units is waypoint index and fraction part is part to the next waypoint 
        public abstract Quaternion GetRotation(float distance);
        public abstract Vector3 GetDirectionAtDistance(float distance);
        public abstract Vector3 GetTangent(float t);
        public abstract Vector3 GetWaypoint(int i);
        public abstract float GetClosestDistanceAlongPath(Vector3 worldPoint);
        public abstract void Generate(Vector3[] points);
        public abstract float Length { get; }
    }
}

