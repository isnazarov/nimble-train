﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    [Serializable]
    public class RailroadSwitch
    {
        [SerializeField] private Connection connLeft;
        [SerializeField] private Connection connRight;

        public Connection ConnLeft
        {
            get
            {
                return connLeft;
            }

            set
            {
                if (value == null)
                {
                    Clear(Direction.left);
                }
                else
                {
                    SetConnection(value, Direction.left);
                }
            }
        }

        public Connection ConnRight {
            get
            {
                return connRight;
            }

            set
            {
                if (value == null)
                {
                    Clear(Direction.right);
                }
                else
                {
                    SetConnection(value, Direction.right);
                }
            }
        }

        public Connection CurrentConnection
        {
            get
            {
                if (IsConsistent)
                {
                    if (currentDirection == Direction.left)
                    {
                        return connLeft;
                    }
                    else
                    {
                        return connRight;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        [SerializeField]
        private bool isConsistent;
        public bool IsConsistent { get { return isConsistent; } private set { isConsistent = value; } } // true when both connections are consistent

        [SerializeField] public Direction currentDirection = Direction.left;

        public RailroadSwitch()
        {
            connLeft = null;
            connRight = null;
            IsConsistent = false;
        }

        public void SetConnection(Connection connection, Direction direction)
        {
            if (connection == null)
            {
                throw new ArgumentNullException();
            }
            if (direction == Direction.left)
            {
                if (connRight == connection)
                {
                    throw new ArgumentException("This connection is already used in the other direction");
                }
                connLeft = connection;
            }
            if (direction == Direction.right)
            {
                if (connLeft == connection)
                {
                    throw new ArgumentException("This connection is already used in the other direction");
                }
                connRight = connection;
            }
            CheckConsistency();
        }

        public void Clear()
        {
            connLeft = null;
            connRight = null;
            IsConsistent = false;
        }

        public void Clear(Direction direction)
        {
            if (direction == Direction.left)
            {
                connLeft = null;
            }
            if (direction == Direction.right)
            {
                connRight = null;
            }
            IsConsistent = false;
        }

        public void CheckConsistency ()
        {
            if (connLeft.IsConsistent && connRight.IsConsistent)
            {
                IsConsistent = true;
            }
            else
            {
                IsConsistent = false;
            }
        }

        public void SetDirection (Direction direction)
        {
            currentDirection = direction;
        }

        /*private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Detector"))
            {
                if (!railroadSwitchButton.isActiveAndEnabled)
                {
                    railroadSwitchButton.gameObject.SetActive(true);
                    railroadSwitchButton.Direction = CurrentDirection;
                }
                else
                {
                    CurrentDirection = railroadSwitchButton.Direction;
                    railroadSwitchButton.gameObject.SetActive(false);
                    //PathManager.Instance.SetNextPathDirection(CurrentDirection);
                    // ... set direction somehow
                }
            }
        }*/

    }

}
 