﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using PathCreation.Examples;
using PathCreation;

namespace NimbleTrain
{
    public class PathCreatorSpriteShapeGenerator : PathSceneTool
    {
        //[SerializeField] private SpriteShapeController railwaySpriteShapePrefab;
        [SerializeField] private SpriteShapeController railsSpriteShapePrefab;
        [SerializeField] private SpriteShapeController sleepersSpriteShapePrefab;
        [SerializeField] private int segmentResolution = 10;
        //private Path path;
        //[SerializeField, HideInInspector] private SpriteShapeController _shape;
        [SerializeField, HideInInspector] private SpriteShapeController _railsShape;
        [SerializeField, HideInInspector] private SpriteShapeController _sleepersShape;

        public SpriteShapeController RailsShape => _railsShape;
        public SpriteShapeController SleepersShape => _sleepersShape;


        protected override void PathUpdated()
        {
            //UpdateShape();
        }

        /*private void Start()
        {
            Initialize();
        }*/

        /*private void OnEnable()
        {
            path.onUpdate += UpdateShape;    
        }*/

        /*private void OnDisable()
        {
            path.onUpdate -= UpdateShape;
        }*/

        /*protected override void OnDestroy()
        {
            base.OnDestroy();
            DestroyShapes();
        }*/

        public void UpdateShape()
        {
            //DestroyShapes();
            if (pathCreator == null)
            {
                pathCreator = GetComponent<PathCreator>();
            }
            Initialize();
        }

        private void DestroyShapes()
        {
            /*if (_shape != null)
            {
                Destroy(_shape.gameObject);
            }*/
            if (_railsShape != null)
            {
                Destroy(_railsShape.gameObject);
            }
            if (_sleepersShape != null)
            {
                Destroy(_sleepersShape.gameObject);
            }
        }

        private void Initialize()
        {
            if (_railsShape == null)
            {
                if (railsSpriteShapePrefab != null)
                {
                    _railsShape = Instantiate(railsSpriteShapePrefab);
                    _railsShape.transform.SetParent(transform, false);
                    _railsShape.transform.localScale = Vector3.one;
                    _railsShape.spline.Clear();
                    _railsShape.spline.isOpenEnded = !path.isClosedLoop;
                }
            }
            if (_sleepersShape == null)
            {
                if (sleepersSpriteShapePrefab != null)
                {
                    _sleepersShape = Instantiate(sleepersSpriteShapePrefab);
                    _sleepersShape.transform.SetParent(transform, false);
                    _sleepersShape.transform.localScale = Vector3.one;
                    _sleepersShape.transform.position += Vector3.down * 0.001f;
                    _sleepersShape.spline.Clear();
                    _sleepersShape.spline.isOpenEnded = !path.isClosedLoop;
                }
            }
            GenerateSpriteShape(_railsShape);
            GenerateSpriteShape(_sleepersShape);
            /*else if (railwaySpriteShapePrefab != null)
            {
                _shape = Instantiate(railwaySpriteShapePrefab);
                _shape.transform.SetParent(transform, false);

                // Made sprite shape prefab 100x larger to avoid problem with rendering 
                // (as recommended here: https://forum.unity.com/threads/spriteshape-modifying-the-shape-at-runtime-the-sprite-does-not-show.586453/) 
                // and scaled back with corresponding parent when instantiated.
                _shape.transform.localScale = Vector3.one;

                _shape.spline.Clear();
                _shape.spline.isOpenEnded = !path.isClosedLoop;
                GenerateSpriteShape(_shape);
            }*/
        }


        private void GenerateSpriteShape(SpriteShapeController shape)
        {
            shape.spline.Clear();
            int index = 0;
            for (int i = 0; i < path.NumPoints; i++)
            {
                /*if (path is CinemachinePathAdapter)
                {
                    for (int j = 0; j < segmentResolution; j++)
                    {
                        float pathUnits = i + 0.1f * j;
                        Vector3 position = path.GetPositionAtPathUnit(pathUnits);
                        Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                        shape.spline.InsertPointAt(index, orientedPosition);
                        index++;
                        if (shape.spline.isOpenEnded && i == path.WpCount - 1)
                        {
                            break;
                        }
                    }
                }*/
                //if (path is BezierPathAdapter)
                //{
                //Vector3 position = path.GetPositionAtPathUnit(i) - path.transform.position;
                Vector3 position = path.GetPoint(Mathf.RoundToInt(i)) - transform.position;
                Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                shape.spline.InsertPointAt(index, orientedPosition);
                index++;
                //}
            }
            Smoothen(shape);
            
            //shape.RefreshSpriteShape();
        }

        private void Smoothen(SpriteShapeController shape)
        {
            int indexOffest = shape.spline.isOpenEnded ? 1 : 0;
            for (int pointIndex = indexOffest; pointIndex < shape.spline.GetPointCount() - indexOffest; pointIndex++)
            {
                Vector3 position = shape.spline.GetPosition(pointIndex);
                Vector3 positionNext = shape.spline.GetPosition(SplineUtility.NextIndex(pointIndex, shape.spline.GetPointCount()));
                Vector3 positionPrev = shape.spline.GetPosition(SplineUtility.PreviousIndex(pointIndex, shape.spline.GetPointCount()));
                Vector3 forward = gameObject.transform.forward;

                float scale = Mathf.Min((positionNext - position).magnitude, (positionPrev - position).magnitude) * 0.33f;

                Vector3 leftTangent = (positionPrev - position).normalized * scale;
                Vector3 rightTangent = (positionNext - position).normalized * scale;

                shape.spline.SetTangentMode(pointIndex, ShapeTangentMode.Continuous);
                SplineUtility.CalculateTangents(position, positionPrev, positionNext, forward, scale, out rightTangent, out leftTangent);

                shape.spline.SetLeftTangent(pointIndex, leftTangent);
                shape.spline.SetRightTangent(pointIndex, rightTangent);
            }

            
        }

        
    }
}
