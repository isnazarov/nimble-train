﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PathCreation.Examples;
using PathCreation;
using System;

namespace NimbleTrain
{
    public abstract class PathConnector : PathSceneTool
    {

        public Path previousPath;
        public Path nextPath;
        public Path connectedPath;

        [HideInInspector]
        public List<Connection> connections = new List<Connection>();

        public RailroadSwitch switchStart = new RailroadSwitch();
        public SwitchLever switchLeverStart;
        public RailroadSwitch switchEnd = new RailroadSwitch();
        public SwitchLever switchLeverEnd;

        protected abstract void AdaptToPreviousPath(); // connect the start point to the end point of the other path
        protected abstract void AdaptToNextPath(); // connect the end point to the start point of the other path
        protected abstract void AutoConnectToPath(Path connectedPath);

        private Path _path;

        public Connection CurrentEndConnection
        {
            get
            {
                if (switchEnd.IsConsistent)
                {
                    return switchEnd.CurrentConnection;
                }
                return connections.Find(conn => (conn.GetConnectionPoint(_path) == 1) && conn.IsConsistent);
            }
        }

        public Connection CurrentStartConnection
        {
            get
            {
                if (switchStart.IsConsistent)
                {
                    return switchStart.CurrentConnection;
                }
                return connections.Find(conn => (conn.GetConnectionPoint(_path) == 0) && conn.IsConsistent);
            }
        }

        private void Awake()
        {
            _path = GetComponent<Path>();
            Initialize(); // We initialize switch levers here because it should happen before PathManager sets the active path 
        }


        // OnEnable and OnDisable fire few times when playmode starts or stops. This is just Unity behaviour but it makes problem
        // because it doesn't synchronize with serialization. So when playmode starts it appears that some connections can contain null path 
        // and when we stop playing some connections can contain paths that already destroyed.
        // To avoid this we use null-check path in UpdateConsistency()

        private void OnEnable()
        {
            UpdateConsistency();
        }

        private void OnDisable()
        {
            UpdateConsistency();
        }

        public void Initialize()
        {
            if (Application.isPlaying)
            {
                if (switchStart.IsConsistent)
                {
                    switchLeverStart.gameObject.SetActive(false);
                    switchLeverStart.Toggle(switchStart.currentDirection);
                    //switchLeverStart.OnToggle -= switchStart.SetDirection;
                    switchLeverStart.OnToggle += switchStart.SetDirection;
                }
                /*else
                {
                    switchLeverStart.gameObject.SetActive(false);
                }*/

                if (switchEnd.IsConsistent)
                {
                    switchLeverEnd.gameObject.SetActive(false);
                    switchLeverEnd.Toggle(switchEnd.currentDirection);
                    //switchLeverEnd.OnToggle -= switchEnd.SetDirection;
                    switchLeverEnd.OnToggle += switchEnd.SetDirection;
                }
                /*else
                {
                    switchLeverEnd.gameObject.SetActive(false);
                }*/
            }
        }


        public void UpdateConsistency()
        {
            foreach (Connection conn in connections)
            {
                Path otherPath = conn.GetOtherPath(this._path);
                if (otherPath != null)
                {
                    otherPath.Connector.switchStart.CheckConsistency();
                    otherPath.Connector.switchEnd.CheckConsistency();
                    if (!Application.isPlaying) // Because we don't want active levers on non-current paths in playmode. 
                    {
                        otherPath.Connector.switchLeverStart.gameObject.SetActive(otherPath.Connector.switchStart.IsConsistent);
                        otherPath.Connector.switchLeverEnd.gameObject.SetActive(otherPath.Connector.switchEnd.IsConsistent);
                    }
                    else // But we want activate levers on current path.
                    {
                        if (PathManager.Instance != null)
                        {
                            if (otherPath == PathManager.Instance.CurrentPath)
                            {
                                PathManager.Instance.ActivateSwitchLever();
                            }
                        }
                    }
                }
            }
        }


        public Connection GetCurrentConnectionAtPoint(int p)
        {
            if (p == 0)
            {
                return CurrentStartConnection;
            }
            else
            {
                return CurrentEndConnection;
            }
        }

        protected override void PathUpdated()
        {
#if UNITY_EDITOR
            if (pathCreator != null)
            {
                if (PrefabUtility.GetPrefabInstanceStatus(gameObject) == PrefabInstanceStatus.Connected)
                {
                    PrefabUtility.UnpackPrefabInstance(gameObject, PrefabUnpackMode.OutermostRoot, InteractionMode.AutomatedAction);
                }
                AdaptTerminalControlPoints();
            }
#endif
        }
        

        protected void AdaptTerminalControlPoints()
        {
            if (previousPath != null)
            {
                AdaptToPreviousPath();
            }

            if (nextPath != null)
            {
                AdaptToNextPath();
            }

            if (connectedPath != null)
            {
                AutoConnectToPath(connectedPath);
            }
        }

        public static void SynchronizeAllConnectionsInScene()
        {
            foreach (BezierPathConnector bezierPathConnector in FindObjectsOfType<BezierPathConnector>())
            {
                bezierPathConnector.CleanupConnections();
            }
        }

        public void CleanupConnections()
        {
            List<Connection> connectionsToDelete = new List<Connection>();
            foreach (Connection conn in connections)
            {
                Path otherPath = conn.GetOtherPath(GetComponent<Path>());
                if (otherPath.GetComponent<PathConnector>().connections.Find(x => x == conn) == null)
                {
                    connectionsToDelete.Add(conn);
                }
            }
            foreach (Connection c in connectionsToDelete)
            {
                connections.Remove(c);
            }
        }

        protected void AddConnection(Connection connection)
        {
            Path otherPath = connection.GetOtherPath(this.GetComponent<Path>());
            if (otherPath == null)
            {
                throw new ArgumentException("Connection argument does not contain this object's Path");
            }
            if (!connections.Contains(connection))
            {
                connections.Add(connection);
            }
            var otherPathConnector = otherPath.GetComponent<PathConnector>();
            if (!otherPathConnector.connections.Contains(connection))
            {
                otherPathConnector.connections.Add(connection);
            }
        }

        public void AddConnectionToSwitch(Connection connection, Direction direction)
        {
            if (connection.GetConnectionPoint(GetComponent<Path>()) == 0)
            {
                if (direction == Direction.left)
                {
                    if (switchStart.ConnLeft == connection)
                    {
                        switchStart.ConnLeft = null;
                    }
                    else
                    {
                        switchStart.ConnLeft = connection;
                    }

                    if (switchStart.ConnRight == connection) switchStart.ConnRight = null;
                }
                else
                {
                    if (switchStart.ConnRight == connection)
                    {
                        switchStart.ConnRight = null;
                    }
                    else
                    {
                        switchStart.ConnRight = connection;
                    }
                    if (switchStart.ConnLeft == connection) switchStart.ConnLeft = null;
                }
            }
            else
            {
                if (direction == Direction.left)
                {
                    if (switchEnd.ConnLeft == connection)
                    {
                        switchEnd.ConnLeft = null;
                    }
                    else
                    {
                        switchEnd.ConnLeft = connection;
                    }
                    if (switchEnd.ConnRight == connection) switchEnd.ConnRight = null;
                }
                else
                {
                    if (switchEnd.ConnRight == connection)
                    {
                        switchEnd.ConnRight = null;
                    }
                    else
                    {
                        switchEnd.ConnRight = connection;
                    }
                    if (switchEnd.ConnLeft == connection) switchEnd.ConnLeft = null;
                }

            }
            switchLeverStart.gameObject.SetActive(switchStart.IsConsistent);
            switchLeverEnd.gameObject.SetActive(switchEnd.IsConsistent);
        }
        
    } 
    
}
