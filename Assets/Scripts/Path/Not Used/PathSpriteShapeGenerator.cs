﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace NimbleTrain
{
    [RequireComponent(typeof(Path))]
    public class PathSpriteShapeGenerator : MonoBehaviour
    {
        [SerializeField] private SpriteShapeController railwaySpriteShapePrefab;
        [SerializeField] private SpriteShapeController railsSpriteShapePrefab;
        [SerializeField] private SpriteShapeController sleepersSpriteShapePrefab;
        [SerializeField] private int segmentResolution = 10;
        private Path _path;
        [SerializeField, HideInInspector] private SpriteShapeController _shape;
        [SerializeField, HideInInspector] private SpriteShapeController _railsShape;
        [SerializeField, HideInInspector] private SpriteShapeController _sleepersShape;

        private void Awake()
        {
            _path = GetComponent<Path>();
        }

        private void Start()
        {
            Initialize();
        }

        private void OnEnable()
        {
            _path.onUpdate += UpdateShape;    
        }

        private void OnDisable()
        {
            _path.onUpdate -= UpdateShape;
        }

        private void OnDestroy()
        {
            DestroyShapes();
        }

        public void UpdateShape()
        {
            DestroyShapes();
            Initialize();
        }

        private void DestroyShapes()
        {
            if (_shape != null)
            {
                Destroy(_shape.gameObject);
            }
            if (_railsShape != null)
            {
                Destroy(_railsShape.gameObject);
            }
            if (_sleepersShape != null)
            {
                Destroy(_sleepersShape.gameObject);
            }
        }

        private void Initialize()
        {
            if ((railsSpriteShapePrefab != null) && (sleepersSpriteShapePrefab != null ))
            {
                _railsShape = Instantiate(railsSpriteShapePrefab);
                _railsShape.transform.SetParent(transform, false);
                _railsShape.transform.localScale = Vector3.one;
                _railsShape.spline.Clear();
                _railsShape.spline.isOpenEnded = !_path.isLooped;

                _sleepersShape = Instantiate(sleepersSpriteShapePrefab);
                _sleepersShape.transform.SetParent(transform, false);
                _sleepersShape.transform.localScale = Vector3.one;
                _sleepersShape.transform.position += Vector3.down * 0.001f;
                _sleepersShape.spline.Clear();
                _sleepersShape.spline.isOpenEnded = !_path.isLooped;

                GenerateSpriteShape(_railsShape);
                GenerateSpriteShape(_sleepersShape);
            }
            else if (railwaySpriteShapePrefab != null)
            {
                _shape = Instantiate(railwaySpriteShapePrefab);
                _shape.transform.SetParent(transform, false);

                // Made sprite shape prefab 100x larger to avoid problem with rendering 
                // (as recommended here: https://forum.unity.com/threads/spriteshape-modifying-the-shape-at-runtime-the-sprite-does-not-show.586453/) 
                // and scaled back with corresponding parent when instantiated.
                _shape.transform.localScale = Vector3.one;

                _shape.spline.Clear();
                _shape.spline.isOpenEnded = !_path.isLooped;
                GenerateSpriteShape(_shape);
            }
        }


        private void GenerateSpriteShape(SpriteShapeController shape)
        {
            int index = 0;
            for (int i = 0; i < _path.WpCount; i++)
            {
                if (_path is CinemachinePathAdapter)
                {
                    for (int j = 0; j < segmentResolution; j++)
                    {
                        float pathUnits = i + 0.1f * j;
                        Vector3 position = _path.GetPositionAtPathUnit(pathUnits);
                        Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                        shape.spline.InsertPointAt(index, orientedPosition);
                        index++;
                        if (shape.spline.isOpenEnded && i == _path.WpCount - 1)
                        {
                            break;
                        }
                    }
                }
                if (_path is BezierPathAdapter)
                {
                    Vector3 position = _path.GetPositionAtPathUnit(i) - _path.transform.position;
                    Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                    shape.spline.InsertPointAt(index, orientedPosition);
                    index++;
                }
            }
            Smoothen(shape);
            
            //shape.RefreshSpriteShape();
        }

        private void Smoothen(SpriteShapeController shape)
        {
            int indexOffest = shape.spline.isOpenEnded ? 1 : 0;
            for (int pointIndex = indexOffest; pointIndex < shape.spline.GetPointCount() - indexOffest; pointIndex++)
            {
                Vector3 position = shape.spline.GetPosition(pointIndex);
                Vector3 positionNext = shape.spline.GetPosition(SplineUtility.NextIndex(pointIndex, shape.spline.GetPointCount()));
                Vector3 positionPrev = shape.spline.GetPosition(SplineUtility.PreviousIndex(pointIndex, shape.spline.GetPointCount()));
                Vector3 forward = gameObject.transform.forward;

                float scale = Mathf.Min((positionNext - position).magnitude, (positionPrev - position).magnitude) * 0.33f;

                Vector3 leftTangent = (positionPrev - position).normalized * scale;
                Vector3 rightTangent = (positionNext - position).normalized * scale;

                shape.spline.SetTangentMode(pointIndex, ShapeTangentMode.Continuous);
                SplineUtility.CalculateTangents(position, positionPrev, positionNext, forward, scale, out rightTangent, out leftTangent);

                shape.spline.SetLeftTangent(pointIndex, leftTangent);
                shape.spline.SetRightTangent(pointIndex, rightTangent);
            }

            
        }

    }
}
