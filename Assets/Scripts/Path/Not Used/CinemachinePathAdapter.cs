﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Cinemachine;

namespace NimbleTrain
{
    public class CinemachinePathAdapter : Path
    {
        private CinemachineSmoothPath m_Path;

        public override int WpCount => m_Path.m_Waypoints.Length;
        public override bool isLooped => m_Path.Looped;

        public override event Action onUpdate = delegate { };

        public override float Length { get { return m_Path.MaxUnit(CinemachinePathBase.PositionUnits.Distance); } }

        private void Awake()
        {
            m_Path = GetComponent<CinemachineSmoothPath>();
        }

        private void OnEnable()
        {
            UpdatePath();
        }

        public override Vector3 GetWaypoint(int i)
        {
            return m_Path.m_Waypoints[i].position;
        }

        public override Vector3 GetPosition(float distance)
        {
            distance = m_Path.StandardizeUnit(distance, CinemachinePathBase.PositionUnits.Distance);
            return m_Path.EvaluatePositionAtUnit(distance, CinemachinePathBase.PositionUnits.Distance);

        }

        public override Quaternion GetRotation(float distance)
        {
            distance = m_Path.StandardizeUnit(distance, CinemachinePathBase.PositionUnits.Distance);
            return m_Path.EvaluateOrientationAtUnit(distance, CinemachinePathBase.PositionUnits.Distance);
        }

        public void UpdatePath()
        {
            onUpdate?.Invoke();
        }

        public override void Generate(Vector3[] points)
        {
            var cinemachineWaypoints = new CinemachineSmoothPath.Waypoint[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                var wp = new CinemachineSmoothPath.Waypoint();
                wp.position = points[i];
                cinemachineWaypoints[i] = wp;
            }
            m_Path.m_Waypoints = cinemachineWaypoints;
            m_Path.InvalidateDistanceCache();
            UpdatePath();
        }

        public override Vector3 GetTangent (float t)
        {
            return m_Path.EvaluateTangentAtUnit(t, CinemachinePathBase.PositionUnits.Normalized).normalized;
        }

        public override Vector3 GetPositionAtPathUnit(float unit)
        {
            return m_Path.EvaluatePositionAtUnit(unit, CinemachinePathBase.PositionUnits.PathUnits);
        }

        public override Vector3 GetDirectionAtDistance(float distance)
        {
            throw new NotImplementedException();
            //if this method is needed try this:  return m_Path.EvaluateTangentAtUnit(distance, CinemachinePathBase.PositionUnits.Distance).normalized;
        }

        public override float GetClosestDistanceAlongPath(Vector3 worldPoint)
        {
            throw new NotImplementedException();
        }
    }

}
