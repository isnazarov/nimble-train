﻿using System;
using UnityEngine;

namespace NimbleTrain
{
    public interface IPath
    {
        Vector3 GetPosition(float distance);
        Quaternion GetRotation(float distance);
        float Length { get; }
        void Generate(Vector3[] points);
        event Action onUpdate;
    } 
}
