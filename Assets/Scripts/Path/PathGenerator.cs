﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class PathGenerator : MonoBehaviour
    {
        [SerializeField] private Path pathPrefab;

        [SerializeField, Range(10, 100)] private int pathSegments;

        [SerializeField, Range(10f, 100f)] private float pathSegmentLengthFactor;
        [SerializeField, Range(10f, 120f)] private float pathTurnAngleFactor;

        private Path currentPath;
        private Path nextPath;
        private Path previousPath;

        public static PathGenerator Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void GeneratePath()
        {
            if (currentPath == null)
            {
                currentPath = Instantiate(pathPrefab);
                currentPath.transform.SetParent(transform);
                InitialPath();
            }
            else
            {
                if (nextPath == null)
                {
                    nextPath = Instantiate(pathPrefab);
                    nextPath.transform.SetParent(transform);
                    GenerateNextPath();
                }
                if (previousPath != null)
                {
                    Destroy(previousPath.gameObject);
                }
                previousPath = currentPath;
                currentPath = nextPath;
                //nextPath = Instantiate(pathPrefab);
                //nextPath.transform.SetParent(transform);
            }
            nextPath = Instantiate(pathPrefab);
            nextPath.transform.SetParent(transform);
            GenerateNextPath();
        }

        private void InitialPath()
        {
            InitialPath(Vector3.zero, Vector3.forward);
        }

        private void InitialPath(Vector3 startPos, Vector3 direction)
        {
            direction.Normalize();
            Vector3 pos = startPos;
            var list = new List<Vector3>();
            for (int i = 0; i < pathSegments; i++)
            {
                list.Add(pos);
                pos += direction * pathSegmentLengthFactor;
            }
            currentPath.Generate(list.ToArray());
        }

        private void GenerateNextPath()
        {
            var list = new List<Vector3>();
            Vector3 tangent = currentPath.GetTangent(1f);
            for (int i = 0; i < 4; i++)
            {
                Vector3 newPosition = currentPath.GetWaypoint(currentPath.WpCount - 1) - currentPath.transform.position + tangent * pathSegmentLengthFactor * i;
                list.Add(newPosition);
            }
            nextPath.Generate(list.ToArray());
            // !!! think how to add other points not recalculating each time
            // It works extremely slow with bezier path!
            /*for (int i = 0; i < pathSegments - 4; i++)
            {
                GenerateNextWaypoint(nextPath);
            }*/
        }

        public void GenerateNextWaypoint(Path path)
        {
            //Vector3 tangent = path.EvaluateTangentAtUnit(1f, CinemachinePathBase.PositionUnits.Normalized).normalized;
            float angle = Random.Range(-pathTurnAngleFactor, pathTurnAngleFactor);
            Vector3 rotatedTangent = Quaternion.AngleAxis(angle, Vector3.up) * path.GetTangent(1f) * pathSegmentLengthFactor;
            //Vector3 newPosition = path.EvaluatePositionAtUnit(1f, CinemachinePathBase.PositionUnits.Normalized) - path.transform.position + rotatedTangent;
            Vector3 newPosition = path.GetWaypoint(path.WpCount - 1) - path.transform.position + rotatedTangent;
            var wpList = new List<Vector3>();
            for (int i = 0; i < path.WpCount; i++)
            {
                wpList.Add(path.GetWaypoint(i));
            }
            wpList.Add(newPosition);
            path.Generate(wpList.ToArray());
            //UpdateSpriteShape(path);
        }

        
    }
}
