﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Threading;
using UnityEngine.U2D;

namespace NimbleTrain
{
    public class PathManager : MonoBehaviour
    {
        public Path initialPath;
        public bool initialIsReverseDirection;

        private Path _currentPath;
        private bool _isReverseDirection;

        public Path CurrentPath => _currentPath;
        public bool IsReverseDirection => _isReverseDirection;


        public static PathManager Instance { get; private set; }

        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            _currentPath = initialPath;
            _isReverseDirection = initialIsReverseDirection;
        }

        private void Start()
        {
            SetNextActivePath(_currentPath, _isReverseDirection);
        }


        public Path GetNextPath(Path path, ref bool isReverseDirection)
        {
            Connection connection = path.Connector.GetCurrentConnectionAtPoint(isReverseDirection ? 0 : 1);
            if (connection != null)
            {
                Path nextPath = connection.GetOtherPath(path);
                isReverseDirection = connection.GetConnectionPoint(nextPath) == 0 ? false : true;
                return nextPath;
            }
            else
            {
                return null;
            }
        }

        public Path GetPreviousPath(Path path, ref bool isReverseDirection)
        {
            Connection connection = path.Connector.GetCurrentConnectionAtPoint(isReverseDirection ? 1 : 0);
            if (connection != null)
            {
                Path previousPath = connection.GetOtherPath(path);
                isReverseDirection = connection.GetConnectionPoint(previousPath) == 0 ? true : false;
                return previousPath;
            }
            else
            {
                return null;
            }    
        }

        public void SetNextActivePath(Path path, bool isReverseDirection)
        {
            _currentPath = path;
            _isReverseDirection = isReverseDirection;

            ActivateSwitchLever(path, isReverseDirection);

            bool isRevDir = isReverseDirection; // just to protect parameter
            Path previousPath = GetPreviousPath(path, ref isRevDir);
            if (previousPath != null)
            {
                previousPath.Connector.switchLeverStart.gameObject.SetActive(false);
                previousPath.Connector.switchLeverEnd.gameObject.SetActive(false);
            }
        }

        public void ActivateSwitchLever(Path path, bool isReverseDirection)
        {
            if (isReverseDirection)
            {
                path.Connector.switchLeverStart.gameObject.SetActive(path.Connector.switchStart.IsConsistent);
            }
            else if (!isReverseDirection)
            {
                path.Connector.switchLeverEnd.gameObject.SetActive(path.Connector.switchEnd.IsConsistent);
            }
        }

        public void ActivateSwitchLever()
        {
            ActivateSwitchLever(_currentPath, _isReverseDirection);
        }

    }
}

