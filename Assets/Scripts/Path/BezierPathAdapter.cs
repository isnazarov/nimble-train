﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using PathCreation.Utility;

namespace NimbleTrain
{
    public class BezierPathAdapter : Path
    {
        private PathCreator _pathCreator;

        public override int WpCount => _pathCreator.path.NumPoints;

        public override bool isLooped => _pathCreator.path.isClosedLoop;

        public override float Length => _pathCreator.path.length;

        public override event Action onUpdate = delegate { };


        private PathConnector _connector;
        public override PathConnector Connector
        {
            get
            {
                if (_connector == null)
                {
                    _connector = GetComponent<PathConnector>();
                }
                return _connector;
            }
        }

        private void Awake()
        {
            _pathCreator = GetComponent<PathCreator>();
            _connector = GetComponent<PathConnector>();
        }

        public override void Generate(Vector3[] points)
        {
            List<Vector3> pts = new List<Vector3>();
            for (int i = 0; i < points.Length; i++)
            {
                pts.Add(points[i]);
            }
            Generate(pts);
        }

        public void Generate(List<Vector3> points)
        {
            _pathCreator.bezierPath = new BezierPath(points, false, PathSpace.xz);
        }

        public override Vector3 GetPosition(float distance)
        {
            return _pathCreator.path.GetPointAtDistance(distance);
        }

        public override Vector3 GetPositionAtPathUnit(float unit)
        {
            /*Vector3 pointPos = _pathCreator.bezierPath.GetPoint((int)unit);
            if (!_pathCreator.path.isClosedLoop && (int)unit == (_pathCreator.bezierPath.NumPoints - 1))
            {
                return pointPos;
            }
            Vector3 nextPointPos = _pathCreator.bezierPath.GetPoint((int)unit == (_pathCreator.bezierPath.NumPoints - 1) ? 0 : (int)unit + 1);
            float distanceToPoint = _pathCreator.path.GetClosestDistanceAlongPath(pointPos);
            float distanceAccordingDecimalPartOfUnits = (_pathCreator.path.GetClosestDistanceAlongPath(nextPointPos) - distanceToPoint) * (unit - (int)unit);
            return _pathCreator.path.GetPointAtDistance(distanceToPoint + distanceAccordingDecimalPartOfUnits);
            */

            // use just vertex path points hoping that its resolution is enough  
            return _pathCreator.path.GetPoint(Mathf.RoundToInt(unit));
        }

        public override Quaternion GetRotation(float distance)
        {
            //return _pathCreator.path.GetRotationAtDistance(distance); // No, because calculated normal (local up) is not global up 
            return Quaternion.LookRotation(_pathCreator.path.GetDirectionAtDistance(distance));
        }

        public override Vector3 GetTangent(float t)
        {
            return _pathCreator.path.GetDirection(t);
        }

        public override Vector3 GetWaypoint(int i)
        {
            return _pathCreator.bezierPath.GetPoint(i);
        }

        public override Vector3 GetDirectionAtDistance(float distance)
        {
            return _pathCreator.path.GetDirectionAtDistance(distance);
        }

        public override float GetClosestDistanceAlongPath(Vector3 worldPoint)
        {
            return _pathCreator.path.GetClosestDistanceAlongPath(worldPoint);
        }
    }

}
