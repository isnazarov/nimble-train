﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Taken from here: https://www.youtube.com/watch?v=raQ3iHhE_Kk&feature=emb_title

[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> listeners = new List<GameEventListener>();

    public void Raise()
    {
        // We need reverse counter to make listeners to be able to remove themselves from the list (unsubscribe) and not get OutOfRange list exception
        for (int i = listeners.Count - 1; i >= 0; i--) 
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        listeners.Remove(listener);
    }
}
