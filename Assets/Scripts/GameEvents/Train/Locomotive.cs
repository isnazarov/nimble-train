﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class Locomotive : MonoBehaviour
    {
        [SerializeField, Range(0.1f, 10f)] private float _acceleration;
        [SerializeField, Range(0.1f, 10f)] private float _brake;
        [SerializeField, Range(1f, 20f)] private float _maxSpeed;
        public float Acceleration { get { return _acceleration; } private set { _acceleration = value; } }
        public float Brake { get { return _brake; } private set { _brake = value; } }
        public float MaxSpeed { get { return _maxSpeed; } private set { _maxSpeed = value; } }

        [SerializeField] private Detector detector;

        private Wagon _wagon;

        private void Awake()
        {
            if (detector != null)
            {
                //detector.OnDetectedLoadingBay += OnLoadingBayDetected;
                //detector.OnDetectedUnloadingBay += OnUnloadingBayDetected;
                detector.OnDetectedStation += OnStationDetected;
                detector.OnDetectedDepot += OnDepotDetected;
            }
            _wagon = GetComponent<Wagon>();
            _wagon.OnChangePath += ChangePath;
        }

        private void OnDisable()
        {
            _wagon.OnChangePath -= ChangePath;
            //detector.OnDetectedLoadingBay -= OnLoadingBayDetected;
            //detector.OnDetectedUnloadingBay -= OnUnloadingBayDetected;
            detector.OnDetectedStation -= OnStationDetected;
            detector.OnDetectedDepot -= OnDepotDetected;
        }

        private void ChangePath ()
        {
            PathManager.Instance.SetNextActivePath(_wagon.Platform.m_Path, _wagon.Platform.m_isReverseDirection);
        }

        /*private void OnLoadingBayDetected(Collider col)
        {
            Train.Instance.InitiateLoadingProcess();
        }

        private void OnUnloadingBayDetected(Collider col)
        {
            Train.Instance.InitiateUnloadingProcess();
        }*/

        private void OnStationDetected(Station station)
        {
            Train.Instance.OnStationArrived(station);
        }

        private void OnDepotDetected()
        {
            Train.Instance.OnDepotArrived();
        }
    } 
}
