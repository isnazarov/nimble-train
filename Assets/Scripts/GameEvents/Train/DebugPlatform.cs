﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [RequireComponent(typeof(Platform))]
    public class DebugPlatform : MonoBehaviour
    {
        [SerializeField, Range(0f, 5f)] private float acceleration;
        private Platform platform;

        private void Awake()
        {
            platform = GetComponent<Platform>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            platform.m_Speed += acceleration * Time.deltaTime;
        }
    }

}