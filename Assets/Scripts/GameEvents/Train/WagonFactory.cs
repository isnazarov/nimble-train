﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [System.Serializable]
    public enum WagonType
    {
        Steam1,
        Wood,
        Cargo
    }

    [System.Serializable]
    public enum LocomotiveType
    {
        Steam1
    }

    public class WagonFactory : MonoBehaviour
    {
        /*[SerializeField] private Locomotive steam1LocomotivePrefab;

        [SerializeField] private Wagon cargoWagonPrefab;
        [SerializeField] private Wagon passengerWagonPrefab;
        */

        [SerializeField] private WagonConfig steamLocomotiveCfg;
        [SerializeField] private WagonConfig woodWagonCfg;
        [SerializeField] private WagonConfig cargoWagonCfg;
        

        public static WagonFactory Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public Wagon CreateWagon()
        {
            return CreateWagon(WagonType.Cargo);
        }

        public Wagon CreateWagon(WagonType wagonType)
        {
            return GetWagonConfig(wagonType).CreateWagon();
        }

        /*public Wagon CreateWagon(WagonType wagonType, Vector3 position, Quaternion rotation)
        {
            return GetWagonConfig(wagonType).CreateWagon(position, rotation);
        }*/

        public Locomotive CreateLocomotive()
        {
            return CreateLocomotive(LocomotiveType.Steam1);
        }

        public Locomotive CreateLocomotive(LocomotiveType locomotiveType)
        {
            //return GetLocomotiveConfig(locomotiveType).CreateLocomotive();
            return null;
        }

        /*private Wagon GetWagonPrefab (WagonType wagonType)
        {
            switch (wagonType)
            {
                case WagonType.Cargo: return cargoWagonPrefab;
                case WagonType.Passenger: return passengerWagonPrefab;
                default: return null;
            }
        }

        private Locomotive GetLocomotivePrefab(LocomotiveType locomotiveType)
        {
            switch (locomotiveType)
            {
                case LocomotiveType.Steam1: return steam1LocomotivePrefab;
                default: return null;
            }
        }*/

        public WagonConfig GetWagonConfig(WagonType wagonType)
        {
            switch (wagonType)
            {
                case WagonType.Steam1: return steamLocomotiveCfg;
                case WagonType.Cargo: return cargoWagonCfg;
                case WagonType.Wood: return woodWagonCfg;
                default: return null;
            }
        }

        public WagonConfig GetLocomotiveConfig(LocomotiveType locomotiveType)
        {
            switch (locomotiveType)
            {
                case LocomotiveType.Steam1: return steamLocomotiveCfg;
                default: return null;
            }
        }
    } 
}
