﻿using UnityEngine;
using UnityEngine.Serialization;
//using Cinemachine;
using System;


namespace NimbleTrain
{
    [ExecuteAlways]

    [RequireComponent(typeof(Rigidbody))]
    public class Platform : MonoBehaviour
    {
        /// <summary>The path to follow</summary>
        [Tooltip("The path to follow")]
        //public CinemachinePathBase m_Path;
        public Path m_Path;
        public bool m_isReverseDirection = false;

        /// <summary>How to interpret the Path Position</summary>
        //[Tooltip("How to interpret the Path Position.  If set to Path Units, values are as follows: 0 represents the first waypoint on the path, 1 is the second, and so on.  Values in-between are points on the path in between the waypoints.  If set to Distance, then Path Position represents distance along the path.")]
        //public CinemachinePathBase.PositionUnits m_PositionUnits = CinemachinePathBase.PositionUnits.Distance;

        /// <summary>Move the cart with this speed</summary>
        [Tooltip("Move the cart with this speed along the path.  The value is interpreted according to the Position Units setting.")]
        [FormerlySerializedAs("m_Velocity")]
        public float m_Speed;

        /// <summary>The cart's current position on the path, in distance units</summary>
        [Tooltip("The position along the path at which the cart will be placed.  This can be animated directly or, if the velocity is non-zero, will be updated automatically.  The value is interpreted according to the Position Units setting.")]
        [FormerlySerializedAs("m_CurrentDistance")]
        public float m_Position;

        private Rigidbody rb;

        public event Action OnChangePath = delegate { };

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            if (!m_isReverseDirection)
            {
                MoveCartToPosition(m_Position + m_Speed * Time.deltaTime);
            }
            else
            {
                MoveCartToPosition(m_Position - m_Speed * Time.deltaTime);
            }
        }

        void LateUpdate()
        {
            if (!Application.isPlaying)
                SetCartPosition(m_Position);
        }

        public void SetCartPosition(float distanceAlongPath)
        {
            if (m_Path != null)
            {
                m_Position = distanceAlongPath;
                transform.position = m_Path.GetPosition(distanceAlongPath);
                //transform.rotation = m_Path.GetRotation(distanceAlongPath);
                transform.rotation = Quaternion.LookRotation((m_isReverseDirection ? -1 : 1) * m_Path.GetDirectionAtDistance(distanceAlongPath));
            }
        }

        void MoveCartToPosition(float distanceAlongPath)
        {
            if (m_Path != null)
            {
                if (!m_Path.isLooped)
                {
                    if (!m_isReverseDirection && distanceAlongPath >= m_Path.Length)
                    {
                        Path nextPath = PathManager.Instance.GetNextPath(m_Path, ref m_isReverseDirection);
                        if (nextPath != null)
                        {
                            float distanceExcessive = distanceAlongPath - m_Path.Length;
                            distanceAlongPath = m_isReverseDirection ? (nextPath.Length - distanceExcessive) : distanceExcessive;
                            m_Path = nextPath;
                            OnChangePath?.Invoke();
                        }
                        else
                        {
                            distanceAlongPath = m_Path.Length;
                        }
                    }
                    else if (m_isReverseDirection && distanceAlongPath <= 0f)
                    {
                        Path nextPath = PathManager.Instance.GetNextPath(m_Path, ref m_isReverseDirection);
                        if (nextPath != null)
                        {
                            float distanceExcessive = Mathf.Abs(distanceAlongPath);
                            distanceAlongPath = m_isReverseDirection ? (nextPath.Length - distanceExcessive) : distanceExcessive;
                            m_Path = nextPath;
                            OnChangePath?.Invoke();
                        }
                        else
                        {
                            distanceAlongPath = 0f;
                        }
                    }
                }

                m_Position = distanceAlongPath;
                rb.MovePosition(m_Path.GetPosition(m_Position));
                // Quaternion.Inverse is useless! It just changes eulers angles signes! It makes no sense!
                /*Quaternion rotation = m_Path.GetRotation(m_Position);
                if (isReverseDirection)
                {
                    rotation = Quaternion.Inverse(rotation);
                }
                rb.MoveRotation(rotation);*/
                if (!m_isReverseDirection)
                {
                    rb.MoveRotation(Quaternion.LookRotation(m_Path.GetDirectionAtDistance(m_Position)));
                }
                else
                {
                    rb.MoveRotation(Quaternion.LookRotation(-m_Path.GetDirectionAtDistance(m_Position)));
                }
            }
        }


        /*void MoveCartToPosition(float distanceAlongPath)
        {
            if (m_Path != null)
            {
                if (!isReverseDirection)
                {
                    if (!m_Path.isLooped)
                    {
                        if (distanceAlongPath >= m_Path.Length)
                        {
                            Connection connection = m_Path.Connector.CurrentEndConnection;
                            if (connection != null)
                            {
                                float distanceExcessive = distanceAlongPath - m_Path.Length;
                                m_Path = connection.GetOtherPath(m_Path);
                                if (connection.GetConnectionPoint(m_Path) == 0)
                                {
                                    isReverseDirection = false;
                                    distanceAlongPath = distanceExcessive;
                                }
                                else
                                {
                                    isReverseDirection = true;
                                    distanceAlongPath = m_Path.Length - distanceExcessive;
                                }
                                OnChangePath?.Invoke();
                                //m_Path.Connector.UpdateSwitchLevers(connection);
                            }
                            else
                            {
                                distanceAlongPath = m_Path.Length;
                            }
                        }
                    }
                }
                else
                {
                    if (!m_Path.isLooped)
                    {
                        if (distanceAlongPath <= 0f)
                        {
                            Connection connection = m_Path.Connector.CurrentStartConnection;
                            if (connection != null)
                            {
                                float distanceExcessive = Mathf.Abs(distanceAlongPath);
                                m_Path = connection.GetOtherPath(m_Path);
                                if (connection.GetConnectionPoint(m_Path) == 0)
                                {
                                    isReverseDirection = false;
                                    distanceAlongPath = distanceExcessive;
                                }
                                else
                                {
                                    isReverseDirection = true;
                                    distanceAlongPath = m_Path.Length - distanceExcessive;
                                }
                                OnChangePath?.Invoke();
                                //m_Path.Connector.UpdateSwitchLevers(connection);
                            }
                            else
                            {
                                distanceAlongPath = 0f;
                            }
                        }
                    }
                }

                if (!isReverseDirection)
                {
                    rb.MoveRotation(Quaternion.LookRotation(m_Path.GetDirectionAtDistance(m_Position)));
                }
                else
                {
                    rb.MoveRotation(Quaternion.LookRotation(-m_Path.GetDirectionAtDistance(m_Position)));
                }
            }
        }*/



    } 
}

