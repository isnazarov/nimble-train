﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

namespace NimbleTrain
{
    public class Detector : MonoBehaviour
    {
        public event Action<Station> OnDetectedStation = delegate { };
        public event Action OnDetectedDepot = delegate { };

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Loading Bay"))
            {
                OnDetectedStation?.Invoke(other.GetComponent<Station>());
            }
            if (other.gameObject.layer == LayerMask.NameToLayer("Depot"))
            {
                OnDetectedDepot?.Invoke();
                other.gameObject.SetActive(false);
            }
            if (other.gameObject.layer == LayerMask.NameToLayer("Object Activator"))
            {
                other.GetComponent<ObjectActivator>().trackedObject.SetActive(true);
            }
        }
    } 
}
