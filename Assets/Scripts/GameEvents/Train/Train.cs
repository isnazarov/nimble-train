﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

namespace NimbleTrain
{
    public class Train : MonoBehaviour
    {
        [SerializeField, Range(0, 10)] private int _maxWagons;
        public int MaxWagons => _maxWagons;
        public float _startPosition;
        [SerializeField, Range(0.1f, 2f)] private float _jointDistance;
        [SerializeField] private bool _useSpringJoints = true;
        [SerializeField, Range(0.1f, 50f)] private float _spring;
        public Locomotive Locomotive { get; private set; }
        public List<Wagon> Wagons { get; private set; } = new List<Wagon>();
        

        [SerializeField] private UnityEvent EventTrainReady;
        [SerializeField] private UnityEvent EventStationStop;
        [SerializeField] private UnityEvent EventDepotStop;
        [SerializeField] private UnityEvent EventTrainCrash;

        [SerializeField] private BuyMenu buyMenu;

        private int _loadedWagonsCounter = 0;

        //private float _acceleration;
        //private float _brake;
        //private float _maxSpeed;

        private float _speed = 0f;

        public bool IsLoading { get; private set; }

        public float JointDistance => _jointDistance;
        public float Spring => _spring;

        //private Camera _camera;
        //[SerializeField] private Vector3 _cameraLocalPosition;
        //[SerializeField] private Vector3 _cameraOffsetPerWagon;


        public event Action<float> OnUpdateSpeed = delegate { };

        //[SerializeField] private bool isTest;
        //[SerializeField] private Locomotive testLocomotive;
        [SerializeField] private List<Wagon> initialWagonsPrefabs;


        public static Train Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            Wagon.OnLoaded += OnWagonLoaded;
        }

        public void Accelerate()
        {
            _speed += Locomotive.Acceleration * Time.deltaTime;
            _speed = Mathf.Min(_speed, Locomotive.MaxSpeed);
        }

        public void Brake()
        {
            _speed -= Locomotive.Brake * Time.deltaTime;
            _speed = Mathf.Max(0f, _speed);
        }


        public void CreateInitialTrain()
        {
            Clear();
            /*if (testWagons.Count > 0)
            {
                foreach (Wagon w in testWagons)
                {
                    AddWagon(w);
                }
                PlaceTrainOnCurrentPath();
            }*/
            foreach (Wagon prefab in initialWagonsPrefabs)
            {
                Wagon w = Instantiate(prefab);
                AddWagon(w);
            }
            PlaceTrainOnCurrentPath();
            OnReady();
        }

        private bool HaveAnyWagonOfCargoType (List<CargoType> cargoTypes)
        {
            if (cargoTypes == null) return false;
            if (cargoTypes.Count <= 0) return false; 
            foreach (CargoType ct in cargoTypes)
            {
                if (HaveAnyWagonOfCargoType(ct)) return true;
            }
            return false;
        }

        private bool HaveAnyWagonOfCargoType (CargoType cargoType)
        {
            foreach (Wagon w in Wagons)
            {
                if (w.GetCargo() != null && w.GetCargo().Type == cargoType) return true;
            }
            return false;
        }

        public void OnStationArrived(Station station)
        {
            StartCoroutine(InitiateStationProcesses(station));
        }

        public void OnDepotArrived()
        {
            StartCoroutine(InitiateDepotProcesses());
        }

        public void OnReady()
        {
            EventTrainReady.Invoke();
        }

        private IEnumerator InitiateDepotProcesses()
        {
            EventDepotStop.Invoke();
            yield return StartCoroutine(StopImmediately());
            _startPosition = Wagons[0].Position;
            buyMenu.gameObject.SetActive(true);
        }

        private IEnumerator InitiateStationProcesses(Station station)
        {
            IsLoading = true;
            if (station.unloadAll)
            {
                yield return StartCoroutine(LoadProcess(Enum.GetValues(typeof(CargoType)).Cast<CargoType>().ToList(), true));
            }
            else if (station.CargoAccepted != null)
            {
                if (HaveAnyWagonOfCargoType(station.CargoAccepted))
                {
                    yield return StartCoroutine(LoadProcess(station.CargoAccepted, true));
                }
            }

            if (station.loadAll)
            {
                yield return StartCoroutine(LoadProcess(Enum.GetValues(typeof(CargoType)).Cast<CargoType>().ToList()));
            }
            else if (station.CargoOffered != null)
            {
                if (HaveAnyWagonOfCargoType(station.CargoOffered))
                {
                    yield return StartCoroutine(LoadProcess(station.CargoOffered));
                }
            }
            IsLoading = false;
            yield return null;
        }

        private IEnumerator LoadProcess(List<CargoType> cargoToLoad, bool unload = false)
        {
            EventStationStop.Invoke();
            GameSession.Instance.TimerStop();
            //yield return StartCoroutine(Stop());
            yield return StartCoroutine(StopImmediately());
            yield return StartCoroutine(LoadWagons(cargoToLoad, unload));
            EventTrainReady.Invoke();
            GameSession.Instance.TimerReset();
            GameSession.Instance.TimerStart();
        }

        private IEnumerator Stop()
        {
            while (_speed > 0)
            {
                Brake();
                Move();
                yield return null;
            }
            yield return null;
        }

        private IEnumerator StopImmediately()
        {
            _speed = 0f;

            // Preventing countable cargo moving by inertia
            foreach (Wagon w in Wagons)
            {
                var c = w.GetComponent<CargoCountable>();
                if (c != null)
                {
                    c.StopImmediately();
                }
            }
            
            Move();
            yield return null;
        }

        public void AllowTilt(bool isTiltAllowed)
        {
            foreach (Wagon w in Wagons)
            {
                w.isTiltAllowed = isTiltAllowed;
            }
        }

        private IEnumerator LoadWagons(List<CargoType> cargoTypes, bool unload = false)
        {
            _loadedWagonsCounter = 0;
            int wagonsToLoad = 0;
            foreach (Wagon w in Wagons)
            {
                if (w.GetCargo() != null && cargoTypes.Contains(w.GetCargo().Type))
                {
                    wagonsToLoad++;
                    if (unload)
                    {
                        w.Unload();
                    }
                    else
                    {
                        w.Load();
                    }
                }
                    
            }
            yield return new WaitUntil(() => _loadedWagonsCounter >= wagonsToLoad);
        }

        private void OnWagonLoaded(Wagon w)
        {
            _loadedWagonsCounter++;
        }

        public void Move()
        {
            OnUpdateSpeed?.Invoke(_speed);
            foreach (Wagon w in Wagons)
            {
                w.Speed = _speed;
            }
            //SetCameraPosition();
        }

        

        public void ResetPosition()
        {
            List<Wagon> wCache = new List<Wagon>(Wagons);
            _speed = 0f;
            Wagons.Clear();
            foreach (Wagon w in wCache)
            {
                //Wagons.Add(w);
                //PlaceWagonOnPath(w);
                //w.AlignPlatform();
                AddWagon(w);
            }
            PlaceTrainOnCurrentPath();
        }

        public void AddWagon(Wagon wagon)
        {
            if (Wagons.Count == 0)
            {
                Locomotive = wagon.GetComponent<Locomotive>();
            }
            Wagons.Add(wagon);
            //PlaceWagonOnPath(wagon);
        }

        public void Detach(Wagon wagon)
        {
            EventTrainCrash.Invoke();
            /*int detachedWagonIndex = Wagons.FindIndex(w => w == wagon);
            if (detachedWagonIndex == 0)
            {
                EventTrainCrash.Invoke();
            }
            for (int i = detachedWagonIndex; i < Wagons.Count; i++)
            {
                Wagons[i].Detach();
            }
            Wagons.RemoveRange(detachedWagonIndex, Wagons.Count - detachedWagonIndex);
            */
        }
        

        private void GenerateNextPath()
        {
            PathGenerator.Instance.GeneratePath();
        }

        public void AddLocomotive(Locomotive locomotive)
        {
            this.Locomotive = locomotive;
            Wagon w = locomotive.GetComponent<Wagon>();
            if (w == null)
            {
                Debug.LogError("No Wagon component in Locomotive object!");
                return;
            }
            AddWagon(w);
        }

        public void PlaceTrainOnCurrentPath()
        {
            PlaceTrainOnCurrentPath(_startPosition);
        }

        public void PlaceTrainOnCurrentPath(float position)
        {
            bool isReversed = PathManager.Instance.IsReverseDirection;
            for (int i = 0; i < Wagons.Count; i++)
            {
                Wagons[i].Path = PathManager.Instance.CurrentPath;
                Wagons[i].IsReverseDirection = isReversed;
                if (i > 0)
                {
                    Wagons[i].Path = Wagons[i - 1].Path;
                    position = CalculateWagonPosition(Wagons[i - 1], Wagons[i]);
                    if (position < 0f || position > Wagons[i].Path.Length)
                    {
                       
                        Path prevPath = PathManager.Instance.GetPreviousPath(PathManager.Instance.CurrentPath, ref isReversed);
                        Wagons[i].Path = prevPath;
                        position = (position < 0f) ? Mathf.Abs(position) : position - PathManager.Instance.CurrentPath.Length;
                        if (!isReversed)
                        {
                            position = prevPath.Length - position;
                        }
                    }
                    if (_useSpringJoints)
                    {
                        Wagons[i].JointTo(Wagons[i - 1]);
                    }
                }
                Wagons[i].Position = position;
            }
        }

        public void PlaceTrainOnCurrentPath(Vector3 closestWorldPoint)
        {
            PlaceTrainOnCurrentPath(PathManager.Instance.CurrentPath.GetClosestDistanceAlongPath(closestWorldPoint));
        }

        private float CalculateWagonPosition(Wagon frontWagon, Wagon backWagon)
        {
            float position;
            bool isReversed = PathManager.Instance.IsReverseDirection;
            if (frontWagon.Path != PathManager.Instance.CurrentPath)
            {
                // the front wagon is on the previous path
                PathManager.Instance.GetPreviousPath(PathManager.Instance.CurrentPath, ref isReversed); // we need only isReversed status
            }
            position = frontWagon.Position + (isReversed ? 1 : -1) * ((frontWagon.Length / 2f) + (backWagon.Length / 2f) + _jointDistance);
            return position;
        }

        

        public void Clear()
        {
            foreach (Wagon w in Wagons)
            {
                Destroy(w.gameObject);
            }
            Wagons.Clear();
            Locomotive = null;
            _speed = 0f;
        }

    }

}