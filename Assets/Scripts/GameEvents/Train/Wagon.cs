﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;
using UnityEngine.Events;

namespace NimbleTrain
{
    public class Wagon : MonoBehaviour
    {
        [SerializeField] private GameObject hull;
        [SerializeField] private Platform platform;
        [SerializeField] private Transform lengthMarkerA;
        [SerializeField] private Transform lengthMarkerB;
        [SerializeField, Range(0.1f, 10f)] private float slowDown;

        [SerializeField] private AnimationCurve centerOfMassCurve;

        //[SerializeField] private float centerOfMassMin;
        //[SerializeField] private float centerOfMassMax;
        [SerializeField] private float maxTiltAngleToCompensate;
        [SerializeField] private float maxTiltAngleToFall;

        //[SerializeField] private UnityEvent onWagonCrash;

        public bool isTiltAllowed = true;

        public float TiltAngle { get; private set; }

        public float TiltAngleSigned
        {
            get
            {
                return Mathf.DeltaAngle(0f, TiltAngle);
            }
        }

        //[HideInInspector] public WagonConfig Config { get; set; }
        public WagonConfig Config;

        private Rigidbody rb;
        private Cargo cargo;
        private CargoUncountable cargoUncountable;
        private bool isCrashed = false;
        private bool isDetached = false;
        private SpringJoint springJoint;

        public static event Action<Wagon> OnLoaded = delegate { };
        public event Action OnChangePath = delegate { };
        //public event Action<Wagon> OnCrash = delegate { };

        public float Position
        {
            get { return platform.m_Position; }
            set
            {
                platform.SetCartPosition(value);
                //hull.transform.SetPositionAndRotation(platform.transform.position, platform.transform.rotation);
                //AlignPlatform();
            }
        }

        public Platform Platform => platform;
        public GameObject Hull => hull;

        public float Speed
        {
            get
            {
                return platform.m_Speed;
            }
            set
            {
                platform.m_Speed = value;
            }
        }

        /*public CinemachineSmoothPath Path
        {
            get { return platform.m_Path as CinemachineSmoothPath; }
            set { platform.m_Path = value; }
        }*/

        public Path Path
        {
            get { return platform.m_Path; }
            set { platform.m_Path = value; }
        }

        public bool IsReverseDirection
        {
            get { return platform.m_isReverseDirection; }
            set { platform.m_isReverseDirection = value; }
        }

        private Bounds bounds;
        public Bounds Bounds
        {
            get
            {
                if (bounds.extents == Vector3.zero)
                {
                    bounds = CalculateBounds(hull);
                }
                return bounds;
            }
            set
            {
                bounds = value;
            }
        }

        private float _length = -1f;
        public float Length// => _length;
        {
            get
            {
                if (_length < 0f)
                {
                    _length = (lengthMarkerA.position - lengthMarkerB.position).magnitude;
                }
                return _length;
                //return Bounds.size.z;
            }
        }

        private void Awake()
        {
            cargo = GetComponent<Cargo>();
            if (cargo != null)
            {
                cargo.OnLoaded += LoadingFinished;
                cargoUncountable = cargo as CargoUncountable;
            }
        }

        private void Start()
        {
            //AlignPlatform();
            platform.OnChangePath += ChangePath;
            rb = hull.GetComponent<Rigidbody>();
            //rb.centerOfMass = Vector3.up * centerOfMassMax;
            rb.centerOfMass = Vector3.up * centerOfMassCurve.Evaluate(0);
        }

        private void FixedUpdate()
        {
            if (!isDetached)
            {
                if (!isCrashed && isTiltAllowed)
                {
                    TiltAngle = hull.transform.rotation.eulerAngles.z;
                    ChangeCenterOfMassVertical();
                    CorrectPosition(TiltAngle);
                    CheckIfFallenDown(); // should go last
                }
                if (!isTiltAllowed)
                {
                    hull.transform.rotation = Quaternion.Euler(0f, hull.transform.rotation.eulerAngles.y, 0f);
                    CorrectPosition(0f);
                }
            }
        }

        private void Update()
        {
            if (isDetached)
            {
                SlowDown();
            }
        }

        private void OnDisable()
        {
            if (cargo != null)
            {
                cargo.OnLoaded -= LoadingFinished;
            }
        }

        public void CalculateLength()
        {
            _length = Bounds.size.z;
        }

        public Cargo GetCargo ()
        {
            if (cargo == null)
            {
                cargo = GetComponent<Cargo>();
            }
            return cargo;
        }

        public void Load()
        {
            if (cargo != null)
            {
                cargo.Load();
            }
            else
            {
                LoadingFinished();
            }
        }

        public void Unload()
        {
            if (cargo != null)
            {
                cargo.Unload();
            }
            else
            {
                LoadingFinished();
            }
        }

        private void LoadingFinished()
        {
            OnLoaded?.Invoke(this);
        }


        private void CheckIfFallenDown()
        {
            if (Mathf.Abs(TiltAngleSigned) > maxTiltAngleToFall)
            {
                Crash();
            }
        }

        private void Crash()
        {
            isCrashed = true;
            rb.constraints = RigidbodyConstraints.None;
            rb.ResetCenterOfMass();
            //onWagonCrash.Invoke();
            Train.Instance.Detach(this);
        }

        private void SlowDown()
        {
            Speed -= slowDown * Time.deltaTime;
            Speed = Mathf.Max(0f, Speed);
        }

        public void Detach()
        {
            if (!isDetached)
            {
                isDetached = true;
                Destroy(gameObject, 5f);
            }
        }

        /*private void ChangeCenterOfMassVertical()
        {
            float shortestAngle = Mathf.Abs(TiltAngleSigned);
            if (shortestAngle < maxTiltAngleToCompensate)
            {
                float t = 1f - shortestAngle / maxTiltAngleToCompensate;
                float centerOfMassAdditional = 0f;
                if (cargoUncountable != null)
                {
                    centerOfMassAdditional = cargoUncountable.CurrentHeight;
                }
                rb.centerOfMass = Vector3.up * Mathf.Lerp(centerOfMassMin, centerOfMassMax + centerOfMassAdditional, t);
            }
            else
            {
                rb.centerOfMass = Vector3.up * centerOfMassMin;
            }
        }*/

        //the same but using curve
        private void ChangeCenterOfMassVertical()
        {
            float shortestAngle = Mathf.Abs(TiltAngleSigned);
            if (shortestAngle < maxTiltAngleToCompensate)
            {
                float t = shortestAngle / maxTiltAngleToCompensate;
                float centerOfMassAdditional = 0f;
                if (cargoUncountable != null)
                {
                    centerOfMassAdditional = cargoUncountable.CurrentHeight;
                }
                rb.centerOfMass = Vector3.up * (centerOfMassCurve.Evaluate(t) + centerOfMassAdditional);
            }
            else
            {
                rb.centerOfMass = Vector3.up * centerOfMassCurve.Evaluate(1f);
            }
        }

        private void CorrectPosition(float angle)
        {
            // We need to correct position and rotation every physics calculation 
            // because when the wagon tilts and stands on the edge of its bottom box collider, it starts to slide out of the platform.
            // So the friction parameter on the material doesn't work in this case.

            rb.rotation = Quaternion.LookRotation(platform.transform.forward, rb.transform.up);
            if (angle < 90f || angle > 270f)  // to avoid indefinite tangents
            {
                float xOffset = Mathf.Tan(Mathf.Deg2Rad * angle) * rb.position.y;
                Vector3 projection = platform.transform.position - platform.transform.right * xOffset;
                Vector3 correctHullPosition = projection + Vector3.up * rb.position.y;
                rb.position = correctHullPosition;
            }
        }

        private void ChangePath()
        {
            OnChangePath?.Invoke();
        }

        public void AlignPlatform()
        {
            BoxCollider col = platform.GetComponent<BoxCollider>();
            //col.size = new Vector3(Bounds.size.x + (Bounds.size.y * 3f), 1f, Bounds.size.z);
            bounds = CalculateBounds(hull);
            col.size = new Vector3(Bounds.size.x, 1f, Bounds.size.z);
            col.center = new Vector3(col.center.x, -col.size.y / 2f, col.center.z);
            float yOffset = hull.transform.localPosition.y - Bounds.center.y;
            Rigidbody[] rbs = hull.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rb in rbs)
            {
                rb.velocity = Vector3.zero;
            }
            hull.transform.localPosition = platform.transform.localPosition + Vector3.up * (Bounds.extents.y + yOffset);
            hull.transform.localRotation = platform.transform.localRotation;

            /*var cargo = GetComponent<Cargo>();
            if (cargo != null)
            {
                cargo.AlignControlZone();
            }*/

        }

        private Bounds CalculateBounds(params GameObject[] models)
        {
            Bounds bounds = new Bounds(models[0].transform.position, Vector3.zero);
            foreach (GameObject model in models)
            {
                foreach (Renderer r in model.GetComponentsInChildren<Renderer>())
                {
                    bounds.Encapsulate(r.bounds);
                }
            }
            return bounds;
        }

        public void JointTo(Wagon frontWagon)
        {
            springJoint = hull.AddComponent<SpringJoint>();
            springJoint.anchor = new Vector3(0f, 0f, Length / 2);
            springJoint.connectedBody = frontWagon.rb;
            springJoint.autoConfigureConnectedAnchor = false;
            springJoint.connectedAnchor = new Vector3(0f, 0f, -frontWagon.Length / 2);
            springJoint.enablePreprocessing = true;
            springJoint.spring = Train.Instance.Spring;
        }


    } 
}
