﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [CreateAssetMenu(fileName = "Wagon/NewWagonConfig", menuName = "Wagon")]
    public class WagonConfig : ScriptableObject
    {
        public string name;
        public WagonType type;
        public Wagon wagonPrefab;
        public Sprite sprite;
        public int cost;


        public Wagon CreateWagon()
        {
            Wagon w = Instantiate(wagonPrefab);
            w.Config = this;
            return w;
        }

        /*public Wagon CreateWagon(Vector3 position, Quaternion rotation)
        {
            Wagon w = Instantiate(wagonPrefab, position, rotation);
            w.Config = this;
            return w;
        }*/
    }

}