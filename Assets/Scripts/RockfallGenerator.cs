﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockfallGenerator : MonoBehaviour
{
    [SerializeField] private List<Rigidbody> objectPrefabs = new List<Rigidbody>();

    [SerializeField, Range(0.5f, 20f)] private float minInterval;
    [SerializeField, Range(0.5f, 20f)] private float maxInterval;
    [SerializeField, Range(0.5f, 20f)] private float lifeTime;

    [SerializeField, Range(0f, 1000f)] private float minHorizontalForce;
    [SerializeField, Range(0f, 1000f)] private float maxHorizontalForce;

    [SerializeField, Range(0f, 1000f)] private float minTorque;
    [SerializeField, Range(0f, 1000f)] private float maxTorque;

    private float _timer;

    private void OnEnable()
    {
        SetTimer();
    }

    // Update is called once per frame
    void Update()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0f)
        {
            GenerateRock();
            SetTimer();
        }
    }

    private void SetTimer()
    {
        _timer = Random.Range(minInterval, maxInterval);
    }

    private void GenerateRock()
    {
        if (objectPrefabs.Count > 0)
        {
            Rigidbody rb = Instantiate(objectPrefabs[Random.Range(0, objectPrefabs.Count)], transform.position, Quaternion.identity);
            rb.AddForce(transform.forward * Random.Range(minHorizontalForce, maxHorizontalForce));
            rb.AddTorque(transform.right * Random.Range(minTorque, maxTorque));
            Destroy(rb.gameObject, lifeTime);
        }
    }
}
