﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using Cinemachine;

namespace NimbleTrain
{
    public class RailwaySpriteShapeGenerator : MonoBehaviour
    {
        [SerializeField] private SpriteShapeController railwaySpriteShapePrefab;
        [SerializeField] private int segmentResolution = 10;

        public static RailwaySpriteShapeGenerator Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        //
        //Sprite Shape methods
        //

        // Maybe move spriteShape methods to separate static class?
        public SpriteShapeController GenerateSpriteShape(CinemachineSmoothPath path, bool isLooped = false)
        {
            //float debugTime = Time.realtimeSinceStartup;
            SpriteShapeController shape = Instantiate(railwaySpriteShapePrefab);
            shape.transform.SetParent(transform, false);
            shape.spline.Clear();
            shape.spline.isOpenEnded = !isLooped;
            int index = 0;
            for (int i = 0; i < path.m_Waypoints.Length; i++)
            {
                for (int j = 0; j < segmentResolution; j++)
                {
                    float d = i + 0.1f * j;
                    Vector3 position = path.EvaluatePositionAtUnit(d, CinemachinePathBase.PositionUnits.PathUnits);
                    Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                    shape.spline.InsertPointAt(index, orientedPosition);
                    index++;
                    if (shape.spline.isOpenEnded && i == path.m_Waypoints.Length - 1)
                    {
                        break;
                    }
                }
            }
            int indexOffest = shape.spline.isOpenEnded ? 1 : 0;
            for (int i = indexOffest; i < shape.spline.GetPointCount() - indexOffest; i++)
            {
                Smoothen(shape, i);
            }
            //shape.RefreshSpriteShape();
            //Debug.Log(Time.realtimeSinceStartup - debugTime);
            return shape;
        }


        public void UpdateSpriteShape(SpriteShapeController currentSpriteShape, CinemachineSmoothPath path)
        {
            int startIndex = currentSpriteShape.spline.GetPointCount() - 1;
            int index = startIndex;
            for (float pathUnits = path.m_Waypoints.Length - 2; pathUnits <= path.m_Waypoints.Length - 1f; pathUnits += 0.1f)
            {
                //float closestDistance = path.FindClosestPoint(handle.position, 0, -1, 10000);
                //Gizmos.DrawLine(handle.position, path.EvaluatePosition(closestDistance));
                Vector3 position = path.EvaluatePositionAtUnit(pathUnits, CinemachinePathBase.PositionUnits.PathUnits);
                Vector3 orientedPosition = new Vector3(position.x, position.z, -position.y);
                index++;
                currentSpriteShape.spline.InsertPointAt(index, orientedPosition);
            }
            //spriteShape.spline.RemovePointAt(startIndex);
            for (int i = startIndex; i < currentSpriteShape.spline.GetPointCount(); i++)
            {
                Smoothen(currentSpriteShape, i);
            }

            /*for (int i = 0; i < 10; i++)
            {
                spriteShape.spline.RemovePointAt(0);
            }*/

            //spriteShape.RefreshSpriteShape();
        }



        private void Smoothen(SpriteShapeController sc, int pointIndex)
        {
            Vector3 position = sc.spline.GetPosition(pointIndex);
            Vector3 positionNext = sc.spline.GetPosition(SplineUtility.NextIndex(pointIndex, sc.spline.GetPointCount()));
            Vector3 positionPrev = sc.spline.GetPosition(SplineUtility.PreviousIndex(pointIndex, sc.spline.GetPointCount()));
            Vector3 forward = gameObject.transform.forward;

            float scale = Mathf.Min((positionNext - position).magnitude, (positionPrev - position).magnitude) * 0.33f;

            Vector3 leftTangent = (positionPrev - position).normalized * scale;
            Vector3 rightTangent = (positionNext - position).normalized * scale;

            sc.spline.SetTangentMode(pointIndex, ShapeTangentMode.Continuous);
            SplineUtility.CalculateTangents(position, positionPrev, positionNext, forward, scale, out rightTangent, out leftTangent);

            sc.spline.SetLeftTangent(pointIndex, leftTangent);
            sc.spline.SetRightTangent(pointIndex, rightTangent);
        }
    } 
}
