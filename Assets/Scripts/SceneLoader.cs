﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private List<string> scenesToLoad = new List<string>();
    [SerializeField] private List<string> scenesToUnload = new List<string>();

    /*private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Detector"))
        {
            StartCoroutine(ChangeScene());
        }
    }

    private IEnumerator ChangeScene()
    {
        foreach (string sceneName in scenesToLoad)
        {
            if (!SceneManager.GetSceneByName(sceneName).isLoaded)
            {
                SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            }
        }

        foreach (string sceneName in scenesToUnload)
        {
            if (SceneManager.GetSceneByName(sceneName).isLoaded)
            {
                SceneManager.UnloadSceneAsync(sceneName);
            }
        }

        //AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(endScene, LoadSceneMode.Additive);
        //AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(startScene);


        //yield return new WaitUntil(() => asyncLoad.isDone == true);
        yield return null;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawIcon(transform.position, gameObject.name);
        Gizmos.color = Color.yellow;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }

    /*private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("CALLBACK: scene " + scene.name + " was loaded");
    }*/


}
