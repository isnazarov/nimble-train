﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace NimbleTrain
{
    [CustomPropertyDrawer(typeof(Connection))]
    public class ConnectionPropertyDrawer : PropertyDrawer
    {
        /*public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement visualElement = new VisualElement();

            PropertyField path0Field = new PropertyField(property.FindPropertyRelative("path0"));
            PropertyField path1Field = new PropertyField(property.FindPropertyRelative("path1"));
            PropertyField path0ConnectionPointField = new PropertyField(property.FindPropertyRelative("path0ConnectionPoint"));
            PropertyField path1ConnectionPointField = new PropertyField(property.FindPropertyRelative("path1ConnectionPoint"));

            visualElement.Add(path0Field);
            visualElement.Add(path1Field);
            visualElement.Add(path0ConnectionPointField);
            visualElement.Add(path1ConnectionPointField);

            return visualElement;
            //return base.CreatePropertyGUI(property);
        }*/

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //base.OnGUI(position, property, label);
            
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var path0Rect = new Rect(position.x, position.y, 160, position.height);
            var path0ConnectionPointRect = new Rect(position.x + 165, position.y, 20, position.height);
            var path1ConnectionPointRect = new Rect(position.x + 190, position.y, 20, position.height);
            var path1Rect = new Rect(position.x + 215, position.y, position.width - 215, position.height);

            // Draw fields - pass GUIContent.none to each so they are drawn without labels
            GUI.enabled = false;
            EditorGUI.PropertyField(path0Rect, property.FindPropertyRelative("path0"), GUIContent.none);
            EditorGUI.PropertyField(path0ConnectionPointRect, property.FindPropertyRelative("path0ConnectionPoint"), GUIContent.none);
            EditorGUI.PropertyField(path1ConnectionPointRect, property.FindPropertyRelative("path1ConnectionPoint"), GUIContent.none);
            EditorGUI.PropertyField(path1Rect, property.FindPropertyRelative("path1"), GUIContent.none);
            GUI.enabled = true;

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    } 
}
