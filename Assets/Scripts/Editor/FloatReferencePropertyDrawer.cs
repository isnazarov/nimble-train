﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(FloatReference))]
public class FloatReferencePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //base.OnGUI(position, property, label);

        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        //EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Calculate rects
        var typeRect = new Rect(position.x, position.y, 15, position.height);
        var valueRect = new Rect(position.x + 20, position.y, position.width - 20, position.height);

        

        SerializedProperty useConstant = property.FindPropertyRelative("useConstant");
        SerializedProperty constantValue = property.FindPropertyRelative("constantValue");
        int index = EditorGUI.Popup(typeRect, useConstant.boolValue ? 0 : 1, new string[] {"Constant", "Variable"}, EditorStyles.miniPullDown); //EditorStyles.foldoutHeaderIcon

        switch (index)
        {
            case 0:
                useConstant.boolValue = true;
                constantValue.floatValue = EditorGUI.FloatField(valueRect, constantValue.floatValue);
                break;
            case 1:
                useConstant.boolValue = false;
                EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("variable"), GUIContent.none);
                break;
            default:
                break;
        }

        //EditorGUI.EndProperty();
    }
}
