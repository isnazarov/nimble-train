﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NimbleTrain
{
    [CustomEditor(typeof(Wagon))]
    public class WagonEditor : Editor
    {
        private void OnEnable()
        {
            Wagon wagon = (Wagon)target;
            wagon.CalculateLength();
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            Wagon wagon = (Wagon)target;
            if (GUILayout.Button("Align Platform"))
            {
                wagon.AlignPlatform();
            }
        }
    } 
}
