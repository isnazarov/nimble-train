﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace NimbleTrain
{
    [CustomEditor(typeof(PathCreatorSpriteShapeGenerator))]
    public class PathCreatorSpriteShapeGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Update Shape"))
            {
                PathCreatorSpriteShapeGenerator pathSpriteShape = (PathCreatorSpriteShapeGenerator)target;
                pathSpriteShape.UpdateShape();
            }
        }
    }

}