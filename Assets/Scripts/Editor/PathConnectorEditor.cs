﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// Taken from example here https://forum.unity.com/threads/display-a-list-class-with-a-custom-editor-script.227847/

namespace NimbleTrain
{
    [CustomEditor(typeof(PathConnector), true)]
    public class PathConnectorEditor : PathCreation.Examples.PathSceneToolEditor
    {
        private PathConnector pathConnector;
        private SerializedObject sPathConnector;

        private SerializedProperty ThisList;
        private int ListSize;

        protected override void OnEnable()
        {
            base.OnEnable();
            pathConnector = (PathConnector)target;
            sPathConnector = new SerializedObject(pathConnector);
            ThisList = sPathConnector.FindProperty("connections");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            sPathConnector.Update();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Connections:");
            EditorGUILayout.Space();

            for (int i = 0; i < ThisList.arraySize; i++)
            {
                SerializedProperty MyListRef = ThisList.GetArrayElementAtIndex(i);
                /*SerializedProperty sPath0 = MyListRef.FindPropertyRelative("path0");
                SerializedProperty sPath1 = MyListRef.FindPropertyRelative("path1");
                SerializedProperty sPath0ConnectionPoint = MyListRef.FindPropertyRelative("path0ConnectionPoint");
                SerializedProperty sPath1ConnectionPoint = MyListRef.FindPropertyRelative("path1ConnectionPoint");
                */
                
                EditorGUILayout.PropertyField(MyListRef);

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Left"))
                {
                    pathConnector.AddConnectionToSwitch(pathConnector.connections[i], Direction.left);
                }
                if (GUILayout.Button("Right"))
                {
                    pathConnector.AddConnectionToSwitch(pathConnector.connections[i], Direction.right);
                }

                GUILayout.EndHorizontal();
                if (GUILayout.Button("Remove"))
                {
                    if (pathConnector.connections[i] == pathConnector.switchStart.ConnLeft) pathConnector.switchStart.ConnLeft = null;
                    else if (pathConnector.connections[i] == pathConnector.switchStart.ConnRight) pathConnector.switchStart.ConnRight = null;
                    else if (pathConnector.connections[i] == pathConnector.switchEnd.ConnLeft) pathConnector.switchEnd.ConnLeft = null;
                    else if (pathConnector.connections[i] == pathConnector.switchEnd.ConnRight) pathConnector.switchEnd.ConnRight = null;
                    pathConnector.connections.Remove(pathConnector.connections[i]);
                }

                //EditorGUILayout.Space();
                //EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            //Apply the changes to our list
            sPathConnector.ApplyModifiedProperties();



            if (GUILayout.Button("Synchronize Connections Globally"))
            {
                BezierPathConnector.SynchronizeAllConnectionsInScene();
            }
            if (GUILayout.Button("Clear all switches"))
            {
                pathConnector.switchStart.Clear();
                pathConnector.switchEnd.Clear();
            }
            if (GUILayout.Button("Clear all connections"))
            {
                pathConnector.switchStart.Clear();
                pathConnector.switchEnd.Clear();
                pathConnector.connections.Clear();
                pathConnector.connectedPath = null;
                pathConnector.nextPath = null;
                pathConnector.previousPath = null;
                BezierPathConnector.SynchronizeAllConnectionsInScene();
            }
        }
    } 
}
