﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OneTimeEventTrigger : MonoBehaviour
{
    public UnityEvent _event;

    private void OnTriggerEnter(Collider other)
    {
        if (_event != null && other.gameObject.layer == LayerMask.NameToLayer("Detector"))
        {
            _event.Invoke();
            gameObject.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    { 
        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
