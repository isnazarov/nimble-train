﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public class GameSession : MonoBehaviour
    {
        public static GameSession Instance { get; private set; }

        [HideInInspector] public Dictionary<CargoType, int> deliveredCargo = new Dictionary<CargoType, int>();
        [SerializeField] private CargoTaskPanel cargoTaskPanel;
        [SerializeField] private BuyMenu buyMenu;
        [SerializeField] private MessageBox messageBox;
        //[SerializeField] private TutorialMessage tutorialMessage;
        [SerializeField] private TutorialManager tutorialManager;

        public event Action<float> OnUpdateTimer = delegate { };
        public event Action<float> OnUpdateBestTime = delegate { };
        public event Action<int> OnUpdateMoney = delegate { };

        private bool _timerIsStarted = false;
        private float _timer;
        private int _money;
        private float _bestTime;

        public List<SceneReference> subscenes;

        [Header("Cutscenes")]
        [SerializeField] private Cutscene cutsceneOpenQuarry;
        [SerializeField] private Cutscene cutsceneBuildDepot;
        [SerializeField] private Cutscene cutsceneBuildTownHouse1;
        [SerializeField] private Cutscene cutsceneBuildTownHouse2;
        [SerializeField] private Cutscene cutsceneBuildTownHouse3;
        [SerializeField] private Cutscene cutsceneBuildTownWell;
        [SerializeField] private Cutscene cutsceneBuildTownChurch;

        private Queue<Cutscene> _pendingCutscenes = new Queue<Cutscene>();
        private Cutscene _currentlyPlayingCutscene;

        [Header("Win Conditions")]
        [SerializeField] private List<WinCondition> winConditions;
        private int _currentWinCondition = 0;
        public int CurrentWinCondition => _currentWinCondition;

        [SerializeField] private UnityEvent EventCutsceneStart;
        [SerializeField] private UnityEvent EventCutsceneStop;

        [SerializeField] private List<SceneReference> initialScenes = new List<SceneReference>();
        private List<AsyncOperation> initialScenesLoadingOps = new List<AsyncOperation>();
        [SerializeField] private Train train;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            
        }

        void Start()
        {
            UpdateCargoTaskPanel();
            //tutorialMessage.Show("Hold SPACE to accelerate.\nRelease to brake.\nSlow down on turns!!!");
            //tutorialManager.ShowBasics();
            //CreateTrain();
        }

        private void Update()
        {
            UpdateTimer();
            CheckCutscenesToPlay();
        }

        public void StartNewGame()
        {
            _timer = 0f;
            SetMoney(0);
            foreach (CargoType ct in Enum.GetValues(typeof(CargoType)))
            {
                deliveredCargo.Add(ct, 0);
            }
            foreach(SceneReference sceneRef in initialScenes)
            {
                initialScenesLoadingOps.Add(SceneManager.LoadSceneAsync(sceneRef.name, LoadSceneMode.Additive));
            }
            StartCoroutine(WaitForSubcenesLoad());
            train.CreateInitialTrain();
        }

        private IEnumerator WaitForSubcenesLoad()
        {
            foreach(AsyncOperation asyncOp in initialScenesLoadingOps)
            {
                yield return new WaitUntil(() => asyncOp.isDone);
            }
            initialScenesLoadingOps.Clear();
            SaveSystem.Instance.OnSave();
            yield return null;
        }

        private void UpdateTimer()
        {
            if (_timerIsStarted)
            {
                _timer += Time.deltaTime;
                OnUpdateTimer?.Invoke(_timer);
            }
        }

        public void TimerStart()
        {
            _timerIsStarted = true;
        }

        public void TimerStop()
        {
            _timerIsStarted = false;
        }

        public void TimerReset()
        {
            _timer = 0f;
            OnUpdateTimer?.Invoke(_timer);
        }

        public void SetMoney(int m)
        {
            _money = m;
            OnUpdateMoney?.Invoke(_money);
        }

        public int GetMoney()
        {
            return _money;
        }

        public void AddMoney(int m)
        {
            _money += m;
            OnUpdateMoney?.Invoke(_money);
        }

        public void SetBestTime()
        {
            if (_timer < _bestTime)
            {
                _bestTime = _timer;
                OnUpdateBestTime?.Invoke(_bestTime);
            }
        }

        public void AddCargo(CargoType cargoType)
        {
            deliveredCargo[cargoType]++;
            CheckWinCondition();
        }

        private void CheckWinCondition()
        {
            if (_currentWinCondition < winConditions.Count)
            {
                UpdateCargoTaskPanel();
                foreach (CargoTask task in winConditions[_currentWinCondition].cargoTasks)
                {
                    if (deliveredCargo[task.Cargo] < task.Quantity)
                    {
                        return;
                    }
                }
                foreach (CargoTask task in winConditions[_currentWinCondition].cargoTasks)
                {
                    deliveredCargo[task.Cargo] -= task.Quantity;
                }
                OnWinConditionComply();
            }
        }

        private void OnWinConditionComply()
        {
            switch (_currentWinCondition)
            {
                case 0:
                    PlayCutscene(cutsceneBuildTownHouse1);
                    break;
                case 1:
                    PlayCutscene(cutsceneBuildTownHouse2);
                    PlayCutscene(cutsceneBuildDepot);
                    buyMenu.CreateLot(WagonFactory.Instance.GetWagonConfig(WagonType.Wood), 200);
                    //tutorialMessage.Show("Click on railroad switch to change the path!\nYou can visit the depot to buy more wagons!");
                    tutorialManager.ActivateRailroadSwitchTutorial();
                    messageBox.ShowMessage("New wagon is available in depot!");
                    Ads.Instance.isPendingToPlay = true;
                    break;
                case 2:
                    PlayCutscene(cutsceneBuildTownHouse3);
                    PlayCutscene(cutsceneOpenQuarry);
                    messageBox.ShowMessage("New wagon is available in depot!");
                    buyMenu.CreateLot(WagonFactory.Instance.GetWagonConfig(WagonType.Cargo), 100);
                    Ads.Instance.isPendingToPlay = true;
                    break;
                case 3:
                    PlayCutscene(cutsceneBuildTownWell);
                    messageBox.ShowMessage("New wagon is available in depot!");
                    buyMenu.CreateLot(WagonFactory.Instance.GetWagonConfig(WagonType.Cargo), 200);
                    Ads.Instance.isPendingToPlay = true;
                    break;
                case 4:
                    PlayCutscene(cutsceneBuildTownChurch);
                    messageBox.ShowMessage("The town is built!!!");
                    messageBox.ShowMessage("YOU WON!");
                    messageBox.ShowMessage("Congratulations!");
                    Ads.Instance.isPendingToPlay = true;
                    break;
                default:
                    break;
            }
            _currentWinCondition++;
            UpdateCargoTaskPanel();
        }

        private void UpdateCargoTaskPanel()
        {
            if (_currentWinCondition < winConditions.Count)
            {
                foreach (CargoTask task in winConditions[_currentWinCondition].cargoTasks)
                {
                    cargoTaskPanel.SetCounter(task.Cargo, task.Quantity - deliveredCargo[task.Cargo]);
                }
            }
        }

        private void PlayCutscene(Cutscene cutscene)
        {
            _pendingCutscenes.Enqueue(cutscene);
        }

        public void SkipCutscene()
        {
            _currentlyPlayingCutscene.SetAsPlayed();
            _currentlyPlayingCutscene = null;
            foreach (Cutscene c in _pendingCutscenes)
            {
                c.SetAsPlayed();
            }
            _pendingCutscenes.Clear();
            EventCutsceneStop.Invoke();
        }

        private void CheckCutscenesToPlay()
        {
            if (_currentlyPlayingCutscene == null && !Train.Instance.IsLoading)
            {
                if (_pendingCutscenes.Count > 0)
                {
                    StartCoroutine(PlayAllPendingCutscenes());
                }
            }
        }

        private IEnumerator PlayAllPendingCutscenes()
        {
            EventCutsceneStart.Invoke();
            while(_pendingCutscenes.Count > 0)
            {
                _currentlyPlayingCutscene = _pendingCutscenes.Dequeue();
                _currentlyPlayingCutscene.Play();
                yield return new WaitWhile(() => _currentlyPlayingCutscene.IsPlaying);
                _currentlyPlayingCutscene = null;
            }
            EventCutsceneStop.Invoke();
        }

        public void SetCurrentWinCondition(int winConditionIndex)
        {
            for (int i = 0; i < winConditionIndex; i++)
            {
                SetWinConditionAsComplied(i);
            }
            for (int i = winConditionIndex; i < winConditions.Count; i++)
            {
                SetWinConditionAsNotComplied(i);
            }
            _currentWinCondition = winConditionIndex;
            UpdateCargoTaskPanel();
        }

        private void SetWinConditionAsComplied(int winConditionIndex)
        {
            switch (winConditionIndex)
            {
                case 0:
                    cutsceneBuildTownHouse1.SetAsPlayed();
                    break;
                case 1:
                    cutsceneBuildTownHouse2.SetAsPlayed();
                    cutsceneBuildDepot.SetAsPlayed();
                    break;
                case 2:
                    cutsceneBuildTownHouse3.SetAsPlayed();
                    cutsceneOpenQuarry.SetAsPlayed();
                    break;
                case 3:
                    cutsceneBuildTownWell.SetAsPlayed();
                    break;
                /*case 4:
                    PlayCutscene(cutsceneBuildTownChurch);
                    messageBox.ShowMessage("The town is built!!!");
                    messageBox.ShowMessage("YOU WIN!");
                    messageBox.ShowMessage("Congratulations!");
                    break;
                    */
                default:
                    break;
            }
        }

        private void SetWinConditionAsNotComplied(int winConditionIndex)
        {
            switch (winConditionIndex)
            {
                case 0:
                    cutsceneBuildTownHouse1.Prepare();
                    break;
                case 1:
                    cutsceneBuildTownHouse2.Prepare();
                    cutsceneBuildDepot.Prepare();
                    break;
                case 2:
                    cutsceneBuildTownHouse3.Prepare();
                    cutsceneOpenQuarry.Prepare();
                    break;
                case 3:
                    cutsceneBuildTownWell.Prepare();
                    break;
                case 4:
                    cutsceneBuildTownChurch.Prepare();
                    break;
                default:
                    break;
            }
        }


        /*private void CreateTrain()
        {
            Locomotive locomotive = WagonFactory.Instance.CreateLocomotive(WagonFactory.LocomotiveType.Steam1);
            Train.Instance.AddLocomotive(locomotive);
            Wagon w = WagonFactory.Instance.CreateWagon(WagonFactory.WagonType.Cargo);
            Train.Instance.AddWagon(w);
        }*/
    } 
}
