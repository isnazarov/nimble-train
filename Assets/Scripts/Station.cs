﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NimbleTrain
{
    public class Station : MonoBehaviour
    {
        //public bool isLoader = false;
        public bool loadAll = false;
        //public CargoType cargoToLoad;

        //public bool isUnloader = false;
        public bool unloadAll = false;
        //public CargoType cargoToUnload;

        public List<CargoTask> cargoTasks = new List<CargoTask>();

        public List<CargoType> CargoAccepted
        {
            get
            {
                if (cargoTasks.Count == 0)
                {
                    return null;
                }
                else
                {
                    List<CargoType> acceptedCargo = new List<CargoType>();
                    foreach (CargoTask task in cargoTasks)
                    {
                        if (task.IsAccepted) acceptedCargo.Add(task.Cargo);
                    }
                    return acceptedCargo;
                }
            }
        }

        public List<CargoType> CargoOffered
        {
            get
            {
                if (cargoTasks.Count == 0)
                {
                    return null;
                }
                else
                {
                    List<CargoType> offeredCargo = new List<CargoType>();
                    foreach (CargoTask task in cargoTasks)
                    {
                        if (task.IsOffered) offeredCargo.Add(task.Cargo);
                    }
                    return offeredCargo;
                }
            }
        }

        private void OnDrawGizmos()
        {
            //Gizmos.DrawIcon(transform.position, gameObject.name);
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(transform.position, transform.localScale);
        }
        /*private void OnCollisionEnter(Collision collision)
        {
            Debug.Log("collision");
        }*/

        /*private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Loading Bay"))
            {
                // Trigger event
                Debug.Log("Arrived to loading bay");
            }
            Debug.Log("trigger");
        }*/
    } 
}
