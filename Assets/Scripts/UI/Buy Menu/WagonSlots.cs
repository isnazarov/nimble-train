﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NimbleTrain
{
    public class WagonSlots : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (DragNDrop.itemBeingDragged != null)
            {
                DragNDrop.itemBeingDragged.EnterSlotsArea();
                DragNDrop.itemBeingDragged.placeholderParent = transform;
            }
            /*if (eventData.pointerDrag == null)
                return;
            Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
            if (d != null)
            {
                d.placeholderParent = this.transform;
            }*/
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (DragNDrop.itemBeingDragged != null)
            {
                if (DragNDrop.itemBeingDragged.placeholderParent == transform)
                {
                    DragNDrop.itemBeingDragged.placeholderParent = DragNDrop.itemBeingDragged.parentToReturnTo;
                    DragNDrop.itemBeingDragged.LeaveSlotsArea();
                }
            }
            /*if (eventData.pointerDrag == null)
                return;
            Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
            if (d != null && d.placeholderParent == this.transform)
            {
                d.placeholderParent = d.parentToReturnTo;
            }*/
        }

        public void OnDrop(PointerEventData eventData)
        {
            // Do we really need this method?
            /*if (DragNDrop.itemBeingDragged != null)
            {
                DragNDrop.itemBeingDragged.parentToReturnTo = transform;
            }*/
        }
    } 
}
