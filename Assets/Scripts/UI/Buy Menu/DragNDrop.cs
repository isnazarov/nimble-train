﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class DragNDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public static DragNDrop itemBeingDragged;

        //private Vector3 startPosition;
        //public Transform StartParent { get; private set; }
        public int StartParentIndex { get; private set; }

        private CanvasGroup _canvasGroup;
        [HideInInspector] public Transform parentToReturnTo = null;
        [HideInInspector] public Transform placeholderParent = null;

        private GameObject _placeholder = null;
        private bool _isInSlotsArea = true;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        

        public void OnBeginDrag(PointerEventData eventData)
        {
            _isInSlotsArea = true;
            itemBeingDragged = this;
            _placeholder = new GameObject("Placeholder");
            _placeholder.transform.SetParent(transform.parent);
            LayoutElement le = _placeholder.AddComponent<LayoutElement>();
            le.preferredWidth = GetComponent<LayoutElement>().preferredWidth;
            le.preferredHeight = GetComponent<LayoutElement>().preferredHeight;
            le.flexibleWidth = 0;
            le.flexibleHeight = 0;

            StartParentIndex = transform.GetSiblingIndex();
            _placeholder.transform.SetSiblingIndex(StartParentIndex);

            parentToReturnTo = transform.parent;
            placeholderParent = parentToReturnTo;
            transform.SetParent(transform.parent.parent);

            _canvasGroup.blocksRaycasts = false;
        }



        public void OnDrag(PointerEventData eventData)
        {
            transform.position = eventData.position;
            // Need to watch if we outside parent
            /*if (_placeholder.transform.parent != placeholderParent)
                _placeholder.transform.SetParent(placeholderParent);*/
            if (_isInSlotsArea)
            {
                int newSiblingIndex = placeholderParent.childCount;
                for (int i = 0; i < placeholderParent.childCount; i++)
                {
                    if (transform.position.x > placeholderParent.GetChild(i).position.x)
                    {
                        newSiblingIndex = i;
                        if (_placeholder.transform.GetSiblingIndex() < newSiblingIndex)
                            newSiblingIndex--;
                        break;
                    }
                }
                _placeholder.transform.SetSiblingIndex(newSiblingIndex);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            itemBeingDragged = null;
            transform.SetParent(parentToReturnTo);
            transform.SetSiblingIndex(_placeholder.transform.GetSiblingIndex());
            _canvasGroup.blocksRaycasts = true;
            Destroy(_placeholder);
        }

        public void LeaveSlotsArea()
        {
            _isInSlotsArea = false;
            _placeholder.transform.SetSiblingIndex(StartParentIndex);
        }

        public void EnterSlotsArea()
        {
            _isInSlotsArea = true;
        }

        private void OnDestroy()
        {
            if (_placeholder != null)
            {
                Destroy(_placeholder.gameObject);
            }
        }
    } 
}
