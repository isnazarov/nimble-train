﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NimbleTrain
{
    public class Lot : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text nameTextField;
        [SerializeField] private TMP_Text priceTextField;
        [SerializeField] private Button buyButton;
        private WagonConfig wagonConfig;
        public WagonConfig Config => wagonConfig;
        private int price;
        public int Price => price;
        private bool _isSubscribedOnUpdateMoney = false;

        private void Awake()
        {
            if (!_isSubscribedOnUpdateMoney)
            {
                GameSession.Instance.OnUpdateMoney += CheckMoneySufficiency;
                _isSubscribedOnUpdateMoney = true;
            }
        }

        private void OnEnable()
        {
            if (!_isSubscribedOnUpdateMoney)
            {
                GameSession.Instance.OnUpdateMoney += CheckMoneySufficiency;
                _isSubscribedOnUpdateMoney = true;
            }
        }

        private void OnDisable()
        {
            if (_isSubscribedOnUpdateMoney)
            {
                GameSession.Instance.OnUpdateMoney -= CheckMoneySufficiency;
                _isSubscribedOnUpdateMoney = false;
            }
        }

        public void Initialize(WagonConfig wagonConfig, int price)
        {
            this.wagonConfig = wagonConfig;
            this.price = price;
            image.sprite = wagonConfig.sprite;
            nameTextField.text = wagonConfig.name;
            priceTextField.text = "$ " + price.ToString();
        }

        public void Buy()
        {
            if (BuyMenu.Instance.Buy(wagonConfig, price))
            {
                Destroy(gameObject);
            }
        }

        private void CheckMoneySufficiency(int money)
        {
            if (money >= price)
            {
                buyButton.interactable = true;
            }
            else
            {
                buyButton.interactable = false;
            }
        }
    } 
}

