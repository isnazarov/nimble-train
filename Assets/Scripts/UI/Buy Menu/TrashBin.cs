﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class TrashBin : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private Image image;
        public event Action<DragNDrop> OnTrashBinDrop = delegate { };

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void OnDrop(PointerEventData eventData)
        {
            OnTrashBinDrop(DragNDrop.itemBeingDragged);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (DragNDrop.itemBeingDragged != null)
            {
                if (DragNDrop.itemBeingDragged.GetComponent<WagonIcon>())
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0.5f);
        }

    } 
}
