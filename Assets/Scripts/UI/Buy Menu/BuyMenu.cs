﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    public class BuyMenu : MonoBehaviour
    {
        [SerializeField] private Lot lotMenuItemPrefab;
        public Transform lotsParent;
        [SerializeField] private WagonIcon wagonIconPrefab;
        [SerializeField] private Transform wagonIconParent;
        [SerializeField] private Transform locomotiveIconParent;
        [SerializeField] private TrashBin trashBin;

        public static BuyMenu Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnEnable()
        {
            ClearTrainInventoryArea();
            FillTrainInventoryArea();
            trashBin.OnTrashBinDrop += Sell;
        }

        private void OnDisable()
        {
            trashBin.OnTrashBinDrop -= Sell;
        }

        private void Start()
        {
            //CreateLot(WagonFactory.Instance.GetWagonConfig(WagonFactory.WagonType.Cargo), 100);
            //CreateLot(WagonFactory.Instance.GetLocomotiveConfig(WagonFactory.LocomotiveType.Steam1), 0);
            //CreateLot(WagonFactory.Instance.GetWagonConfig(WagonFactory.WagonType.Cargo), 0);
            //CreateLot(WagonFactory.Instance.GetWagonConfig(WagonFactory.WagonType.Cargo), 333);
        }

        private void FillTrainInventoryArea()
        {
            WagonIcon locomotiveIcon = Instantiate(wagonIconPrefab);
            locomotiveIcon.transform.SetParent(locomotiveIconParent);
            locomotiveIcon.Initialize(Train.Instance.Wagons[0].Config);

            for (int i = 1; i < Train.Instance.Wagons.Count; i++)
            {
                WagonIcon wagonIcon = Instantiate(wagonIconPrefab);
                wagonIcon.transform.SetParent(wagonIconParent);
                wagonIcon.Initialize(Train.Instance.Wagons[i].Config);
            }
        }

        private void ClearTrainInventoryArea()
        {
            if (locomotiveIconParent.childCount > 0)
            {
                Destroy(locomotiveIconParent.GetChild(0).gameObject);
            }
            if (wagonIconParent.childCount > 0)
            {
                foreach (Transform t in wagonIconParent)
                {
                    Destroy(t.gameObject);
                }
            }
        }

        public void ClearLots()
        {
            foreach(Transform t in lotsParent)
            {
                Destroy(t.gameObject);
            }
        }

        public void CreateLot(WagonConfig wagonConfig, int price)
        {
            Lot lot = Instantiate(lotMenuItemPrefab, lotsParent); // parent should be specified here to scale the lot properly according to the screen resolution
            lot.Initialize(wagonConfig, price);
        }

        public bool Buy(WagonConfig wagonConfig, int price)
        {
            if (GameSession.Instance.GetMoney() >= price)
            {
                // Check if max possible number of wagons exceeded
                GameSession.Instance.AddMoney(-price);
                
                // Need to instantiate with specifying parent otherwise layout element is not resized
                if (wagonConfig.wagonPrefab.GetComponent<Locomotive>() != null)
                {
                    ClearLocomotiveSlot();
                    WagonIcon wagonIcon = Instantiate(wagonIconPrefab, locomotiveIconParent);
                    wagonIcon.Initialize(wagonConfig);
                }
                else
                {
                    WagonIcon wagonIcon = Instantiate(wagonIconPrefab, wagonIconParent);
                    wagonIcon.Initialize(wagonConfig);
                }
                //... check conditions and if needed add new lots (maybe condition triggered)
                return true;
            }
            return false;
        }

        private void ClearLocomotiveSlot()
        {
            if (locomotiveIconParent.childCount > 0)
            {
                Destroy(locomotiveIconParent.GetChild(0).gameObject);
            }
        }

        public void Sell(DragNDrop draggedItem)
        {
            // dont allow to sell last wagon
            WagonIcon wagonIcon = draggedItem.GetComponent<WagonIcon>();
            if (wagonIcon != null)
            {
                GameSession.Instance.AddMoney((int)(wagonIcon.GetWagonConfig().cost * 0.2f));
                Destroy(wagonIcon.gameObject);
            }
        }

        public void Proceed()
        {
            Train.Instance.Clear();
            WagonConfig locoConfig = locomotiveIconParent.GetChild(0).GetComponent<WagonIcon>().GetWagonConfig();
            Train.Instance.AddWagon(locoConfig.CreateWagon());
            foreach (Transform t in wagonIconParent)
            {
                Train.Instance.AddWagon(t.GetComponent<WagonIcon>().GetWagonConfig().CreateWagon());
            }
            Train.Instance.PlaceTrainOnCurrentPath();
            gameObject.SetActive(false);
            Train.Instance.OnReady();
            Time.timeScale = 1f;
        }
    } 
}
