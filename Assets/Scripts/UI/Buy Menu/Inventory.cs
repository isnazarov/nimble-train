﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NimbleTrain
{
    public class Inventory : MonoBehaviour, IHasChanged
    {
        [SerializeField] Transform slots;

        private List<Transform> _itemsBeforeDrag = new List<Transform>();

        void Start()
        {
            HasChanged();
        }

        public void HasChanged()
        {
            foreach (Transform slotTransform in slots)
            {
                // build local list of wagons
            }
        }

        private void ResetItemsCache()
        {
            if (_itemsBeforeDrag.Count > 0)
            {
                for (int i = 0; i < _itemsBeforeDrag.Count; i++)
                {
                    if (_itemsBeforeDrag[i] != null)
                    {
                        _itemsBeforeDrag[i].SetParent(slots.GetChild(i));
                        //Debug.Log("RESETTING item: " + _itemsBeforeDrag[i].name + " now in " + _itemsBeforeDrag[i].parent.name);
                    }
                }
                _itemsBeforeDrag.Clear();
            }
        }

        private void CacheItems()
        {
            ResetItemsCache();
            foreach (Transform t in slots)
            {
                if (t.childCount > 0)
                {
                    _itemsBeforeDrag.Add(t.GetChild(0));
                }
                else
                {
                    _itemsBeforeDrag.Add(null);
                }
            }
        }

        public void OnHoverItem(GameObject go)
        {
            /*int hoveredSlotIndex = go.transform.GetSiblingIndex();
            int itemSlotIndex = DragNDrop.itemBeingDragged.StartParentIndex;
            if (hoveredSlotIndex == itemSlotIndex)
            {
                return;
            }
            CacheItems();
            int itemsToShift = Mathf.Abs(hoveredSlotIndex - itemSlotIndex);
            int shiftDirection = (hoveredSlotIndex - itemSlotIndex > 0) ? -1 : 1;
            for (int i = 0; i < itemsToShift; i++)
            {
                Transform item = slots.transform.GetChild(hoveredSlotIndex + i * shiftDirection).GetChild(0);
                //Debug.Log("MOVING " + item.name + " from " + (hoveredSlotIndex + i * shiftDirection).ToString()  +  " to " + (hoveredSlotIndex + (i + 1) * shiftDirection).ToString());
                item.SetParent(slots.transform.GetChild(hoveredSlotIndex + (i + 1) * shiftDirection));
                //Debug.Log("MOVED " + item.name + " to " + item.parent.name);
            }

            // Need to fix this
            //DragNDrop.itemBeingDragged.transform.SetParent(go.transform);

            //DragNDrop.itemBeingDragged.StartParent.SetSiblingIndex(hoveredSlotIndex);
            //Debug.Log("OnHoverItem " + hoveredSlotIndex + " " + go.name);
            */
        }

        public void OnHoverExitItem(GameObject go)
        {
            ResetItemsCache();

            //DragNDrop.itemBeingDragged.StartParent.SetSiblingIndex(DragNDrop.itemBeingDragged.StartParentIndex);
            //Debug.Log("OnHoverExitItem " + go.transform.GetSiblingIndex() + " " + go.name);
        }
    }

}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
        void OnHoverItem(GameObject go);
        void OnHoverExitItem(GameObject go);
    }
}