﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class WagonIcon : MonoBehaviour
    {
        [SerializeField] private WagonConfig wagonConfig;
        public bool IsLocomotive { get; private set; }

        public void Initialize(WagonConfig wagonConfig)
        {
            this.wagonConfig = wagonConfig;
            if (wagonConfig.wagonPrefab.GetComponent<Locomotive>() != null)
            {
                IsLocomotive = true;
                Destroy(GetComponent<DragNDrop>());
            }
            else
            {
                IsLocomotive = false;
            }
            GetComponent<Image>().sprite = wagonConfig.sprite;
            gameObject.name = "icon" + wagonConfig.name;
        }

        public WagonConfig GetWagonConfig()
        {
            return wagonConfig;
        }

    } 
}
