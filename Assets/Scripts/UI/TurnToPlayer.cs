﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnToPlayer : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    
    void Update()
    {
        transform.forward = transform.position - _camera.transform.position;
    }
}
