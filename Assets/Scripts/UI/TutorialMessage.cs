﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialMessage : MonoBehaviour
{
    [SerializeField] private TMP_Text textField; 

    public void Show(string message, int fontSize = 40)
    {
        Time.timeScale = 0f;
        gameObject.SetActive(true);
        textField.fontSize = fontSize;
        textField.text = message;
    }

    public void Proceed()
    {
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }
}
