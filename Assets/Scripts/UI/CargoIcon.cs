﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class CargoIcon : MonoBehaviour
    {
        private Camera _camera;
        [SerializeField] private Image _image;

        // Update is called once per frame
        void Update()
        {
            transform.forward = transform.position - _camera.transform.position;
        }

        public void Initialize(Sprite img, Camera cam)
        {
            _image.sprite = img;
            _image.preserveAspect = true;
            _camera = cam;
        }
    } 
}
