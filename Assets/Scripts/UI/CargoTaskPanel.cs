﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace NimbleTrain
{
    public class CargoTaskPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text woodTextField;
        [SerializeField] private TMP_Text stoneTextField;

        public void SetCounter(CargoType type, int n)
        {
            switch(type)
            {
                case CargoType.wood:
                    if (n <= 0)
                    {
                        woodTextField.transform.parent.gameObject.SetActive(false);
                    }
                    else
                    {
                        woodTextField.transform.parent.gameObject.SetActive(true);
                        woodTextField.text = "x " + n.ToString();
                    }
                    break;
                case CargoType.stone:
                    if (n <= 0)
                    {
                        stoneTextField.transform.parent.gameObject.SetActive(false);
                    }
                    else
                    {
                        stoneTextField.transform.parent.gameObject.SetActive(true);
                        stoneTextField.text = "x " + n.ToString();
                    }
                    break;
                default:
                    break;
            }
            
        }
        
    }

}