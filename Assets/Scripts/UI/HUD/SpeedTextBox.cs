﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace NimbleTrain
{
    public class SpeedTextBox : MonoBehaviour
    {
        private TextMeshProUGUI textBox;

        private void Awake()
        {
            textBox = GetComponent<TextMeshProUGUI>();
        }

        private void Start()
        {
            Train.Instance.OnUpdateSpeed += SetText;
        }

        private void SetText(float f)
        {
            SetText(((int)f).ToString());
        }

        private void SetText(string txt)
        {
            textBox.text = txt;
        }
    } 
}
