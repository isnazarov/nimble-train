﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class CargoIconGenerator : MonoBehaviour
    {
        [SerializeField] private CameraController cameraController;
        [SerializeField] private CargoIcon cargoIconPrefab;
        //[SerializeField] private Image cargoIconPrefab;

        public static CargoIconGenerator Instance;

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        /*public void Generate(CargoConfig cargo, Vector3 worldPos)
        {
            Image icon = Instantiate(cargoIconPrefab, cameraController.mainCamera.WorldToScreenPoint(worldPos), Quaternion.identity, transform);
            icon.sprite = cargo.cargoIcon;
            icon.transform.DOMove(icon.transform.position + Vector3.up * 100f, 1f).OnComplete(() => { Destroy(icon.gameObject); });
        }*/

        public void Generate(CargoConfig cargo, Vector3 worldPos)
        {
            CargoIcon icon = Instantiate(cargoIconPrefab,worldPos + Vector3.up, Quaternion.identity, transform);
            icon.Initialize(cargo.cargoIcon, cameraController.mainCamera);
            icon.transform.DOMove(icon.transform.position + Vector3.up * 4f, 1f).OnComplete(() => { Destroy(icon.gameObject); });
        }
    } 
}
