﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace NimbleTrain
{
    public class TimerTextBox : MonoBehaviour
    {
        private TextMeshProUGUI textBox;

        private void Awake()
        {
            textBox = GetComponent<TextMeshProUGUI>();
        }

        private void Start()
        {
            GameSession.Instance.OnUpdateTimer += SetText;
        }

        private void SetText(float f)
        {
            SetText(f.ToString("0.00"));
        }

        private void SetText(string txt)
        {
            textBox.text = txt;
        }
    } 
}
