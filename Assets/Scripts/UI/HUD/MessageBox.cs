﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class MessageBox : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    private Queue<string> _msgQueue = new Queue<string>();
    private bool _isPlaying;

    public void ShowMessage(string text)
    {
        _msgQueue.Enqueue(text);
        Play();
    }

    private void Play()
    {
        if (!_isPlaying)
        {
            _isPlaying = true;
            PlayNext();
        }
    }

    private void PlayNext()
    {
        if (_msgQueue.Count > 0)
        {
            Show();
        }
        else
        {
            _isPlaying = false;
        }
    }

    private void Show()
    {
        _text.text = _msgQueue.Dequeue();
        transform.position = new Vector3(-700f, transform.position.y, transform.position.z);
        transform.DOLocalMoveX(0f, 0.5f).OnComplete(Remove);
    }

    private void Remove()
    {
        transform.DOLocalMoveX(700f, 0.5f).SetDelay(1.5f).OnComplete(PlayNext);
    }
    
}
