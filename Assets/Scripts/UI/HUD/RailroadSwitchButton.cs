﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class RailroadSwitchButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Sprite imgLeft;
        [SerializeField] private Sprite imgRight;
        private Image _image;

        private Direction _direction;
        public Direction Direction {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
                SetImage();
            }
        }

        private void Awake()
        {
            _image = GetComponent<Image>();
        }

        private void SetImage()
        {
            if (Direction == Direction.left)
            {
                _image.sprite = imgLeft;
            }
            else
            {
                _image.sprite = imgRight;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Direction == Direction.left)
            {
                Direction = Direction.right;
            }
            else
            {
                Direction = Direction.left;
            }
        }
    } 
}
