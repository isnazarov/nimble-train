﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace NimbleTrain
{
    public class MoneyTextBox : MonoBehaviour
    {
        private TextMeshProUGUI textBox;

        private void Awake()
        {
            textBox = GetComponent<TextMeshProUGUI>();
        }

        private void Start()
        {
            GameSession.Instance.OnUpdateMoney += SetText;
        }

        private void OnEnable()
        {
            SetText(GameSession.Instance.GetMoney());
        }

        private void SetText(int i)
        {
            SetText(i.ToString());
        }

        private void SetText(string txt)
        {
            textBox.text = "$ " + txt;
        }
    } 
}
