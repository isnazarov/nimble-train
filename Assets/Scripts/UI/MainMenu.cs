﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public class MainMenu : MonoBehaviour
    {
        public Button buttonNewGame;
        public TMP_Text txtStartGameButton;
        public SaveSystem saveSystem;

        private void Awake()
        {
            if (File.Exists(Application.persistentDataPath + "/saves/savegame.save"))
            {
                SceneManager.LoadScene(1);
            }
        }

        /*private void OnEnable()
        {
            Time.timeScale = 0f;
            if (saveSystem.SaveFileExists())
            {
                txtStartGameButton.text = "Continue";
                buttonNewGame.gameObject.SetActive(true);
            }
            else
            {
                txtStartGameButton.text = "Start";
                buttonNewGame.gameObject.SetActive(false);
            }
        }*/

        /*private void OnDisable()
        {
            Time.timeScale = 1f;
        }*/

        public void StartNewGame()
        {
            SceneManager.LoadScene(1);
        }

        /*public void ResetProgress()
        {
            saveSystem.DeleteSaveFile();
            //StartNewGame();
        }*/

        /*public void StartGame()
        {
            if (saveSystem.SaveFileExists())
            {
                saveSystem.OnLoad();
            }
        }*/

        /*public void LoadGame()
        {
            GameManager.Instance.LoadGame();
        }*/
    }

}