﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NimbleTrain
{
    public class PauseMenu : MonoBehaviour
    {
        public SaveSystem saveSystem;

        private void OnEnable()
        {
            Time.timeScale = 0f;
        }

        private void OnDisable()
        {
            Time.timeScale = 1f;
        }

        public void ResetProgress()
        {
            saveSystem.DeleteSaveFile();
            SceneManager.LoadScene(0);
        }
    }

}