﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NimbleTrain
{
    [CreateAssetMenu]
    public class WinCondition : ScriptableObject
    {
        public List<CargoTask> cargoTasks = new List<CargoTask>();
    } 
}
