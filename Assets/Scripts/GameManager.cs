﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace NimbleTrain
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        public GameObject loadingScreen;
        public Slider progressBar;
        private float totalSceneProgress;

        private List<AsyncOperation> scenesLoading = new List<AsyncOperation>();

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        }

        

        public void LoadGame()
        {
            loadingScreen.gameObject.SetActive(true);
            scenesLoading.Add(SceneManager.UnloadSceneAsync(1));
            scenesLoading.Add(SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive));

            StartCoroutine(GetSceneLoadProgress());
        }

        public IEnumerator GetSceneLoadProgress()
        {
            for (int i = 0; i < scenesLoading.Count; i++)
            {
                while(!scenesLoading[i].isDone)
                {
                    totalSceneProgress = 0f;
                    foreach(AsyncOperation operation in scenesLoading)
                    {
                        totalSceneProgress += operation.progress;
                    }
                    totalSceneProgress = totalSceneProgress / scenesLoading.Count;
                    progressBar.value = Mathf.Clamp01(totalSceneProgress);
                    yield return null;
                }
            }

            loadingScreen.gameObject.SetActive(false);
        }
    } 
}
