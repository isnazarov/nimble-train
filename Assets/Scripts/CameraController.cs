﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace NimbleTrain
{
    public class CameraController : MonoBehaviour
    {
        public Camera mainCamera;
        [SerializeField] private CinemachineVirtualCamera vcamDefault;
        [SerializeField] private CinemachineVirtualCamera vcamCliff;
        [SerializeField] private CinemachineVirtualCamera vcamLoading;
        [SerializeField] private CinemachineVirtualCamera vcamRiverCrossingEntrance;
        [SerializeField] private CinemachineVirtualCamera vcamCliffExit;
        [SerializeField] private CinemachineVirtualCamera vcamDepot;
        [SerializeField] private CinemachineVirtualCamera vcamTown;

        [SerializeField] private Train train;

        private CinemachineVirtualCamera _activeCamera;

        private void OnEnable()
        {
            _activeCamera = vcamDefault;
        }

        /*private void SetCameraPosition()
        {
            if (Wagons.Count > 0)
            {
                _camera.transform.LookAt(Wagons[0].Platform.transform);
            }
        }*/

        /*private void PlaceCamera()
        {
            if (Wagons.Count > 0)
            {
                _camera.transform.SetParent(Wagons[Wagons.Count - 1].Platform.transform);
                //_camera.transform.SetParent(Wagons[0].Platform.transform);
                _camera.transform.localPosition = _cameraLocalPosition + _cameraOffsetPerWagon * (Wagons.Count - 1);
            }
        }*/

        public void OnTrainReady()
        {
            vcamDefault.Follow = Train.Instance.Wagons[0].Platform.transform;
            vcamDefault.LookAt = Train.Instance.Wagons[0].Platform.transform;
            SwitchToCamera(vcamDefault);
        }

        public void OnStationStopped()
        {
            int middleWagonIndex = (int)(Train.Instance.Wagons.Count / 2);
            vcamLoading.Follow = Train.Instance.Wagons[middleWagonIndex].Platform.transform;
            vcamLoading.LookAt = Train.Instance.Wagons[middleWagonIndex].Platform.transform;
            SwitchToCamera(vcamLoading);
        }

        public void OnTrainCrash()
        {
            _activeCamera.Follow = null;
            _activeCamera.LookAt = null;
        }


        public void OnCliffReached()
        {
            if (_activeCamera == vcamDefault)
            {
                StartCoroutine(SwitchToVcamCliff());
            }
            else
            {
                SwitchToCamera(vcamDefault);
            }
        }

        private IEnumerator SwitchToVcamCliff()
        {
            CinemachineTrackedDolly dolly = vcamCliff.GetCinemachineComponent<CinemachineTrackedDolly>();
            CinemachineTrackedDolly.AutoDolly ad = dolly.m_AutoDolly;
            ad.m_Enabled = false;
            dolly.m_AutoDolly = ad;
            vcamCliff.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = 0f;
            vcamCliff.Follow = train.Wagons[0].Platform.transform;
            vcamCliff.LookAt = train.Wagons[0].Platform.transform;
            SwitchToCamera(vcamCliff);
            // We should wait until camera place 0th position. Otherwise it stays on the end point of the track. 
            yield return new WaitForSeconds(2f);
            ad.m_Enabled = true;
            dolly.m_AutoDolly = ad;
            yield return null;
        }

        public void SwitchToVcamRiverCrossingEntrance()
        {
            SwitchToCamera(vcamRiverCrossingEntrance);
        }

        public void SwitchToVcamCliffExit()
        {
            SwitchToCamera(vcamCliffExit);
        }

        public void SwitchToVcamDepot()
        {
            SwitchToCamera(vcamDepot);
        }

        public void SwitchToVcamTown()
        {
            SwitchToCamera(vcamTown);
        }

        public void SwitchToVcamDefault()
        {
            SwitchToCamera(vcamDefault);
        }

        private void SwitchToCamera(CinemachineVirtualCamera vcam)
        {
            _activeCamera.Priority = 10;
            _activeCamera = vcam;
            _activeCamera.Priority = 50;
        }


    }

}